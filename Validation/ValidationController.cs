﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace TypedTaggingModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateConvertTypedTagsToRelationsRequest(ConvertTypedTagsToRelationsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is not valid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetConvertTypedTagsToRelationsModel(ConvertTypedTagsToRelationsModel model, OntologyModDBConnector dbReader, string propertyName, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(ConvertTypedTagsToRelationsModel.Config))
            {
                if (!dbReader.Objects1.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Config Found!";
                    return result;
                }

                if (dbReader.Objects1.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Multiple Configs Found!";
                    return result;
                }

                model.Config = dbReader.Objects1.First();
            }
            else if (propertyName == nameof(ConvertTypedTagsToRelationsModel.ConfigsToOntologies))
            {
                if (!dbReader.ObjectRels.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Ontologies related!";
                    return result;
                }

                model.ConfigsToOntologies = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ConvertTypedTagsToRelationsModel.DeleteOtherRelations))
            {
                if (dbReader.ObjAtts.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Only one Attribute to mark delete of other Relations is allowed!";
                    return result;
                }

                model.DeleteOtherRelations = dbReader.ObjAtts.First();
            }
            else if (propertyName == nameof(ConvertTypedTagsToRelationsModel.RelatedClasses))
            {
                if (dbReader.ObjectRels.Any(rel => rel.Ontology != globals.Type_Class))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "There are other types as classes related als related classes!";
                    return result;
                }

                model.RelatedClasses = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();
            }

            return result;
        }
    }
}
