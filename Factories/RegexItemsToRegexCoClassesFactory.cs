﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;
using TypedTaggingModule.Services;

namespace TypedTaggingModule.Factories
{
    public class RegexItemsToRegexCoClassesFactory
    {
        public async Task<ResultItem<List<RegexToClass>>> CreateList(RegexToClassItems regexToClassItems, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<RegexToClass>>>(() =>
            {
            var result = new ResultItem<List<RegexToClass>>
            {
                ResultState = globals.LState_Success.Clone()
            };

                result.Result = (from regexItem in regexToClassItems.RegexToClasses
                                 join regexAtt in regexToClassItems.RegexItems on regexItem.GUID equals regexAtt.ID_Object
                                 join classItem in regexToClassItems.RelatedClasses on regexItem.GUID equals classItem.ID_Object
                                 select new RegexToClass
                                 {
                                     IdAttributeTypeRegex = Config.LocalData.AttributeType_RegEx.GUID,
                                     IdClass = classItem.ID_Other,
                                     NameClass = classItem.Name_Other,
                                     IdClassParent = classItem.ID_Parent_Other,
                                     IdRegexToClass = regexItem.GUID,
                                     NameRegexToClass = regexItem.Name,
                                     Regex = regexAtt.Val_String
                                 }).ToList();

                return result;
            });


            return taskResult;
        }
    }
}
