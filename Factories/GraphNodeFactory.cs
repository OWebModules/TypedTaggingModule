﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace TypedTaggingModule.Factories
{
    public class GraphNodeFactory
    {
        public async Task<ResultItem<PossibleRelations>> CreateGraphNodeList(clsOntologyItem refItem, List<Reference> references, List<clsClassRel> classRelations, List<clsObjectRel> objectRelations, List<clsObjectRel> possibleObjectRelations, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<PossibleRelations>>(() =>
            {
                var result = new ResultItem<PossibleRelations>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new PossibleRelations()
                };

                result.Result.PossibleORelations = possibleObjectRelations;

                var objectReferences = references.SelectMany(refer => refer.Tags).GroupBy(refe => new { GUID = refe.IdTag, Name = refe.NameTag, GUID_Parent = refe.IdTagParent }).Select(refGroup => new clsOntologyItem { GUID = refGroup.Key.GUID, Name = refGroup.Key.Name, GUID_Parent = refGroup.Key.GUID_Parent }).ToList();

                //var classNodes = classRelations.GroupBy(cls => new { GUID = cls.ID_Class_Left, Name = cls.Name_Class_Left }).Select(clsGrp =>
                //    new GraphNode
                //    {
                //        id = clsGrp.Key.GUID,
                //        name = clsGrp.Key.Name,
                //        data = new GraphNodeData
                //        {
                //            color = "#557EAA",
                //            type = "rectangle",
                //            dim = 20
                //        }
                //    }).ToList();

                var rootNode = new GraphNode
                {
                    id = refItem.GUID,
                    name = refItem.Name,
                    IdParent = refItem.GUID_Parent,
                    data = new GraphNodeData
                    {
                        color = "#557EAA",
                        type = "circle",
                        dim = 20
                    }
                };

                //classNodes.AddRange(from classNode in classRelations.GroupBy(cls => new { GUID = cls.ID_Class_Right, Name = cls.Name_Class_Right }).Select(clsGrp =>
                //    new GraphNode
                //    {
                //        id = clsGrp.Key.GUID,
                //        name = clsGrp.Key.Name,
                //        data = new GraphNodeData
                //        {
                //            color = "#557EAA",
                //            type = "rectangle",
                //            dim = 10
                //        }
                //    })
                //                    join clsExist in classNodes on classNode.id equals clsExist.id into clssExist
                //                    from clsExist in clssExist.DefaultIfEmpty()
                //                    where clsExist == null
                //                    select classNode);

                var objectNotes = (from objectItem in objectReferences
                                   join classRel in classRelations on objectItem.GUID_Parent equals classRel.ID_Class_Left
                                   join possibleRelation in possibleObjectRelations on objectItem.GUID equals possibleRelation.ID_Object
                                   join objectExist in objectRelations on objectItem.GUID equals objectExist.ID_Other into objectsExists
                                   from objectExist in objectsExists.DefaultIfEmpty()
                                   //where objectExist == null
                                   //     && classRel.ID_Class_Right == possibleRelation.ID_Parent_Other
                                   select new GraphNode
                                   {
                                       id = objectItem.GUID,
                                       name = objectItem.Name,
                                       IdParent = objectItem.GUID_Parent
                                   }).ToList();

                objectNotes.AddRange(from objectItem in objectReferences
                                     join classRel in classRelations on objectItem.GUID_Parent equals classRel.ID_Class_Right
                                     join possibleRelation in possibleObjectRelations on objectItem.GUID equals possibleRelation.ID_Other
                                       join objectExist in objectRelations on objectItem.GUID equals objectExist.ID_Other into objectsExists
                                       from objectExist in objectsExists.DefaultIfEmpty()
                                       //where objectExist == null
                                       // && classRel.ID_Class_Right == possibleRelation.ID_Parent_Object
                                     select new GraphNode
                                       {
                                           id = objectItem.GUID,
                                           name = objectItem.Name,
                                           IdParent = objectItem.GUID_Parent
                                       });



                var objectNodes = objectNotes.GroupBy(objNode => new { objNode.id, objNode.name, objNode.IdParent }).Select(objNode => new GraphNode
                {
                    id = objNode.Key.id,
                    name = objNode.Key.name,
                    IdParent = objNode.Key.IdParent,
                    data = new GraphNodeData
                    {
                        color = "#557EFF",
                        type = "circle",
                        dim = 20
                    }
                }).ToList();

                var relNodes = (from rel in possibleObjectRelations
                                join otherNode in objectNodes on rel.ID_Other equals otherNode.id
                                join classRel in classRelations on rel.ID_Parent_Object equals classRel.ID_Class_Left
                                select new { rel, otherNode }).ToList();

                relNodes.AddRange(from rel in possibleObjectRelations
                                  join otherNode in objectNodes on rel.ID_Object equals otherNode.id
                                  join classRel in classRelations on rel.ID_Parent_Other equals classRel.ID_Class_Right
                                  select new { rel, otherNode });


           objectNodes.ForEach(objNode =>
                {
                    var relations = relNodes.Where(relNode => relNode.rel.ID_Object == objNode.id).ToList();
                    relations.AddRange(relNodes.Where(relNode => relNode.rel.ID_Other == objNode.id));

                    objNode.adjacencies = relations.Select(rel => new GraphNodeAdjacencies
                    {
                        nodeFrom = objNode.id,
                        nodeTo = rel.otherNode.id,
                        data = new GraphNodeAdjacenciesData
                        {
                            color = "#557EAA"
                        }
                    }).ToList();


                });

                rootNode.adjacencies = objectNodes.Select(objNode => new GraphNodeAdjacencies
                {
                    nodeFrom = rootNode.id,
                    nodeTo = objNode.id,
                    data = new GraphNodeAdjacenciesData
                    {
                        color = "#557EAA"
                    }
                }).ToList();


                result.Result.PossibleRelationNodes = new List<GraphNode>
                {
                    rootNode
                };
                result.Result.PossibleRelationNodes.AddRange(objectNodes);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<GraphNode>>> CreateGraphNodeList(clsOntologyItem refItem, List<Reference> references, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<GraphNode>>>(() =>
            {
                var result = new ResultItem<List<GraphNode>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<GraphNode>()
                };

                var rootNode = new GraphNode
                {
                    id = refItem.GUID,
                    name = refItem.Name,
                    IdParent = refItem.GUID_Parent,
                    data = new GraphNodeData
                    {
                        color = "#557EAA",
                        type = "circle",
                        dim = 20
                    }
                };

                var classNodes = references.SelectMany(itm => itm.Tags).GroupBy(itmGrp => new { itmGrp.IdTagParent, itmGrp.NameTagParent }).Select(tag => new GraphNode
                {
                    id = tag.Key.IdTagParent,
                    name = tag.Key.NameTagParent,
                    data = new GraphNodeData
                    {
                        color = "#9803fc",
                        type = "ellipse",
                        dim = 30
                    }
                }).ToList();

                classNodes.AddRange(references.SelectMany(itm => itm.Tags).GroupBy(itmGrp => new { itmGrp.IdParentTagSource, itmGrp.NameParentTagSource }).Select(tag => new GraphNode
                {
                    id = tag.Key.IdParentTagSource,
                    name = tag.Key.NameParentTagSource,
                    data = new GraphNodeData
                    {
                        color = "#9803fc",
                        type = "ellipse",
                        dim = 30
                    }
                }));

                var objectNotes = references.SelectMany(itm => itm.Tags).Select(tag => new GraphNode
                {
                    id = tag.IdTag,
                    name = tag.NameTag,
                    IdParent = tag.IdTagParent,
                    data = new GraphNodeData
                    {
                        color = "#557EBB",
                        type = "circle",
                        dim = 15
                    }
                }).ToList();

                objectNotes.AddRange(references.SelectMany(itm => itm.Tags).Select(tag => new GraphNode
                {
                    id = tag.IdTagSource,
                    name = tag.NameTagSource,
                    IdParent = tag.IdParentTagSource,
                    data = new GraphNodeData
                    {
                        color = "#557EBB",
                        type = "circle",
                        dim = 15
                    }
                }));

                objectNotes.ForEach(objNode =>
                {
                    var classNode = classNodes.Where(cls => cls.id == objNode.IdParent).FirstOrDefault();
                    if (classNode != null)
                    {
                        objNode.adjacencies.Add(new GraphNodeAdjacencies
                        {
                            nodeFrom = objNode.id,
                            nodeTo = classNode.id,
                            data = new GraphNodeAdjacenciesData
                            {
                                color = "#557EAA"
                            }
                        });
                    }
                });

                rootNode.adjacencies = objectNotes.Select(objNode => new GraphNodeAdjacencies
                {
                    nodeFrom = rootNode.id,
                    nodeTo = objNode.id,
                    data = new GraphNodeAdjacenciesData
                    {
                        color = "#557EAA"
                    }
                }).ToList();


                result.Result.Add(rootNode);
                result.Result.AddRange(objectNotes);
                result.Result.AddRange(classNodes);

                return result;
            });

            return taskResult;
        }
    }
}
