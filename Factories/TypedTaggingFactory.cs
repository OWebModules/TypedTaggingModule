﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;

namespace TypedTaggingModule.Factories
{
    public class TypedTaggingFactory : NotifyPropertyChange
    {
        private object serviceLocker = new object();


        private clsLogStates logStates = new clsLogStates();

        private ResultTypedTags resultTypedTags;
        public ResultTypedTags ResultTypedTags
        {
            get
            {
                lock(serviceLocker)
                {
                    return resultTypedTags;
                }
                
            }
            set
            {
                lock(serviceLocker)
                {
                    resultTypedTags = value;
                }

                RaisePropertyChanged(nameof(ResultTypedTags));
            }
        }

        public async Task<ResultTypedTags> CreateTypedTagsList(List<clsObjectRel> typedTagsToSources,
            List<clsObjectRel> typedTagsToTags)
        {
            var typedTags = (from typedTagToSource in typedTagsToSources
                             join typedTagToTag in typedTagsToTags on typedTagToSource.ID_Object equals typedTagToTag.ID_Object
                             select new TypedTag
                             {
                                 IdTypedTag = typedTagToSource.ID_Object,
                                 NameTypedTag = typedTagToSource.Name_Object,
                                 IdTagSource = typedTagToSource.ID_Other,
                                 NameTagSource = typedTagToSource.Name_Other,
                                 IdParentTagSource = typedTagToSource.ID_Parent_Other,
                                 NameParentTagSource = typedTagToSource.Name_Parent_Other,
                                 IdTag = typedTagToTag.ID_Other,
                                 NameTag = typedTagToTag.Name_Other,
                                 IdTagParent = typedTagToTag.ID_Parent_Other,
                                 NameTagParent = typedTagToTag.Name_Parent_Other,
                                 TagType = typedTagToTag.Ontology,
                                 OrderId = typedTagToSource.OrderID.Value
                             }).ToList();

            var result = new ResultTypedTags
            {
                Result = logStates.LogState_Success.Clone(),
                TypedTags = typedTags
            };

            ResultTypedTags = result;
            return result;
        }
    }

    public class ResultTypedTags
    {
        public clsOntologyItem Result { get; set; }
        public List<TypedTag> TypedTags { get; set; }
    }
}
