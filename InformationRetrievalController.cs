﻿using Accord.MachineLearning.Text.Stemmers;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Models;
using TypedTaggingModule.Services;

namespace TypedTaggingModule
{
    public class InformationRetrievalController : AppController
    {
        public async Task<ResultItem<List<RankedWord>>> GetRankedFoundWords(string text)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<List<RankedWord>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<RankedWord>()
                };

                var elasticAgent = new ServiceAgentElastic(Globals);

                var stopWordsResult = await elasticAgent.GetStopWords();
                result.ResultState = stopWordsResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var parseBase = new ParseBase(Globals);
                var wordResult = await parseBase.FindWords(text);

                var stopWords = stopWordsResult.Result.Select(sw => sw.Name.Replace("\r", ""));
                var foundWordsWithoutStopWords = (from foundWord in wordResult.FoundWords
                                                  join stopWord in stopWords on foundWord.Word.ToLower() equals stopWord.ToLower() into stopWordsOut
                                                  from stopWord in stopWordsOut.DefaultIfEmpty()
                                                  where stopWord == null
                                                  select foundWord).ToList();

                var germanStemmer = new GermanStemmer();

                foundWordsWithoutStopWords.ForEach(fw => fw.Word = germanStemmer.Stem(fw.Word));

                result.Result = foundWordsWithoutStopWords.GroupBy(fw => fw.Word).Select(fw => new RankedWord { Word = fw.Key }).ToList();

                foreach (var rankedWord in result.Result)
                {
                    rankedWord.Count = foundWordsWithoutStopWords.Select(fw => fw.Word).Count(wr => wr == rankedWord.Word);
                }

                var rank = 0;
                var lastCount = 0;
                result.Result = result.Result.OrderByDescending(rw => rw.Count).ToList();
                for (int i = 0; i < result.Result.Count; i++)
                {
                    var rankedWord = result.Result[i];
                    if (lastCount != rankedWord.Count)
                    {
                        rank++;
                    }
                    rankedWord.Rank = rank;
                    lastCount = rankedWord.Count;
                }

                return result;
            });

            return taskResult;
        }
        public InformationRetrievalController(Globals globals) : base(globals)
        {

        }
    }
}
