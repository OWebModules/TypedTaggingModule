﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntologyItemsModule.Services;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Factories;
using TypedTaggingModule.Models;
using TypedTaggingModule.Services;
using TypedTaggingModule.Validation;

namespace TypedTaggingModule.Connectors
{
    public class TypedTaggingConnector : AppController
    {
        private ServiceAgentElastic serviceAgentElastic;

        private object connectorLocker = new object();

        public clsOntologyItem ClassTypedTag
        {
            get
            {
                return Config.LocalData.Class_Typed_Tag;
            }
        }

        private TypedTaggingFactory typedTaggingFactory;

        private ConnectorResultTypedTags resultSaveTypedTags;
        public ConnectorResultTypedTags ResultSaveTypedTags
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultSaveTypedTags;
                }

            }
            set
            {
                lock (connectorLocker)
                {
                    resultSaveTypedTags = value;
                }
            }
        }

        private ConnectorResultTypedTags resultSaveTypedTagsNoView;
        public ConnectorResultTypedTags ResultSaveTypedTagsNoView
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultSaveTypedTagsNoView;
                }

            }
            set
            {
                lock (connectorLocker)
                {
                    resultSaveTypedTagsNoView = value;
                }
            }
        }

        private ConnectorResultTypedTags resultFoundTypedTags;
        public ConnectorResultTypedTags ResultFoundTypedTags
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultFoundTypedTags;
                }
            }
            set
            {
                lock (connectorLocker)
                {
                    resultFoundTypedTags = value;
                }
            }
        }

        private clsOntologyItem resultDeleteTypedTag;
        public clsOntologyItem ResultDeleteTypedTag
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultDeleteTypedTag;
                }

            }
            set
            {
                lock (connectorLocker)
                {
                    resultDeleteTypedTag = value;
                }
            }
        }

        private ResultReorderTags resultReorderTypedTag;
        public ResultReorderTags ResultReorderTypedTag
        {
            get
            {
                lock (connectorLocker)
                {
                    return resultReorderTypedTag;
                }

            }
            set
            {
                lock (connectorLocker)
                {
                    resultReorderTypedTag = value;
                }
            }
        }

        public async Task<ResultItem<ConvertTypedTagsToRelationsResult>> ConvertTypedTagsToRelations(ConvertTypedTagsToRelationsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ConvertTypedTagsToRelationsResult>>(async () =>
           {
               var result = new ResultItem<ConvertTypedTagsToRelationsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new ConvertTypedTagsToRelationsResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");

               result.ResultState = ValidationController.ValidateConvertTypedTagsToRelationsRequest(request, Globals);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               if (request.CancellationToken.IsCancellationRequested)
               {
                   result.ResultState = Globals.LState_Nothing.Clone();
                   result.ResultState.Additional1 = "User-Abort!";
                   request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               request.MessageOutput?.OutputInfo("Get Model...");
               var elasticAgent = new ServiceAgentElastic(Globals);

               var serviceResult = await elasticAgent.GetConvertTypedTagsToRelationsModel(request);

               result.ResultState = serviceResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
               }

               if (request.CancellationToken.IsCancellationRequested)
               {
                   result.ResultState = Globals.LState_Nothing.Clone();
                   result.ResultState.Additional1 = "User-Abort!";
                   request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have Model.");

               var ontologyController = new OntologyConnector(Globals);
               var ontologies = new List<Ontology>();

               request.MessageOutput?.OutputInfo("Get Ontologies...");
               foreach (var ontology in serviceResult.Result.ConfigsToOntologies.Select(rel => new clsOntologyItem { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other, Type = rel.Ontology }).ToList())
               {
                   var getOntologiesRequest = new GetOntologyRequest(ontology);
                   var getOntologiesResult = await ontologyController.GetOntologies(getOntologiesRequest);
                   result.ResultState = getOntologiesResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   ontologies.AddRange(getOntologiesResult.Result);
               }

               if (request.CancellationToken.IsCancellationRequested)
               {
                   result.ResultState = Globals.LState_Nothing.Clone();
                   result.ResultState.Additional1 = "User-Abort!";
                   request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                   return result;
               }

               if (!ontologies.Any())
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Ontologies found!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have {ontologies.Count} Ontologies.");

               request.MessageOutput?.OutputInfo($"Get related joins...");
               var relatedJoins = ontologies.SelectMany(ont => ont.OntologyJoins).ToList()
                    .Where(join => join.OItem1.RefItem.Type == Globals.Type_Class
                                && join.OItem2 != null
                                && join.OItem2.RefItem.Type == Globals.Type_Class
                                && join.OItem3 != null
                                && join.OItem3.RefItem.Type == Globals.Type_RelationType).ToList();

               if (serviceResult.Result.RelatedClasses.Any())
               {
                   relatedJoins = (from related in relatedJoins
                                   join allowedClass in serviceResult.Result.RelatedClasses on related.OItem1.RefItem.GUID equals allowedClass.GUID
                                   select related).ToList();

                   relatedJoins = (from related in relatedJoins
                                   join allowedClass in serviceResult.Result.RelatedClasses on related.OItem2.RefItem.GUID equals allowedClass.GUID
                                   select related).ToList();
               }

               var taggingSourceClasses = relatedJoins.GroupBy(join => join.OItem1.RefItem.GUID).Select(grp => new clsOntologyItem { GUID_Parent = grp.Key }).ToList();

               request.MessageOutput?.OutputInfo($"Have related joins.");

               if (request.CancellationToken.IsCancellationRequested)
               {
                   result.ResultState = Globals.LState_Nothing.Clone();
                   result.ResultState.Additional1 = "User-Abort!";
                   request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                   return result;
               }

               if (!taggingSourceClasses.Any())
               {
                   request.MessageOutput?.OutputInfo("No Taggingsource-Classes!");
                   return result;
               }

               request.MessageOutput?.OutputInfo("Get the tagging-sources...");
               var dbReaderTaggingSources = new OntologyModDBConnector(Globals);

               result.ResultState = dbReaderTaggingSources.GetDataObjects(taggingSourceClasses);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Tagging-Sources!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var taggingSources = dbReaderTaggingSources.Objects1;

               if (!taggingSources.Any())
               {
                   request.MessageOutput?.OutputInfo("No Taggingsources!");
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have {taggingSources.Count} tagging-sources...");

               if (request.CancellationToken.IsCancellationRequested)
               {
                   result.ResultState = Globals.LState_Nothing.Clone();
                   result.ResultState.Additional1 = "User-Abort!";
                   request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                   return result;
               }

               var searchSetAutomatic = taggingSources.Select(source => new clsObjectRel
               {
                   ID_Object = source.GUID,
                   ID_RelationType = ConvertRelations.Config.LocalData.RelationType_Set_Automatic.GUID
               }).ToList();

               request.MessageOutput?.OutputInfo($"Get automatic related Items...");

               var dbReaderSetAutomatic = new OntologyModDBConnector(Globals);

               result.ResultState = dbReaderSetAutomatic.GetDataObjectRel(searchSetAutomatic);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the automatic set related Items!";
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have {dbReaderSetAutomatic.ObjectRels.Count} automatic related items...");

               request.MessageOutput?.OutputInfo("Get Typed-Tags...");
               var getTagsResult = await GetTags(taggingSources, true);

               result.ResultState = getTagsResult.Result;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have {getTagsResult.TypedTags} Typed-Tags.");

               if (request.CancellationToken.IsCancellationRequested)
               {
                   result.ResultState = Globals.LState_Nothing.Clone();
                   result.ResultState.Additional1 = "User-Abort!";
                   request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Get relations to set...");

               var relationConfig = new clsRelationConfig(Globals);
               var relationsToSet = (from typedTag in getTagsResult.TypedTags
                                     join taggingSource in taggingSources on typedTag.IdTagSource equals taggingSource.GUID
                                     join relatedJoin in relatedJoins on typedTag.IdTagParent equals relatedJoin.OItem2.RefItem.GUID
                                     select relationConfig.Rel_ObjectRelation(taggingSource, new clsOntologyItem
                                     {
                                         GUID = typedTag.IdTag,
                                         Name = typedTag.NameTag,
                                         GUID_Parent = typedTag.IdTagParent,
                                         Type = typedTag.TagType
                                     }, relatedJoin.OItem3.RefItem, orderId: typedTag.OrderId)).ToList();

               request.MessageOutput?.OutputInfo($"Have {relationsToSet.Count} relations to set.");

               if (request.CancellationToken.IsCancellationRequested)
               {
                   result.ResultState = Globals.LState_Nothing.Clone();
                   result.ResultState.Additional1 = "User-Abort!";
                   request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Get relations to delete...");
               var automaticSetsToDelete = (from automaticSet in dbReaderSetAutomatic.ObjectRels
                                            join relationToSet in relationsToSet on automaticSet.ID_Other equals relationToSet.ID_Other into relationsToSet1
                                            from relationToSet in relationsToSet1.DefaultIfEmpty()
                                            where relationToSet == null
                                            select automaticSet).ToList();

               var relationsToDelete = new List<clsObjectRel>();

               if (automaticSetsToDelete.Any())
               {
                   var searchRelationsToDelete = (from source in taggingSources
                                                  join automaticSet in automaticSetsToDelete on source.GUID equals automaticSet.ID_Object
                                                  select new clsObjectRel
                                                  {
                                                      ID_Object = source.GUID,
                                                      ID_Other = automaticSet.ID_Other
                                                  }).ToList();

                   var dbConnectorRelationsToDelete = new OntologyModDBConnector(Globals);

                   result.ResultState = dbConnectorRelationsToDelete.GetDataObjectRel(searchRelationsToDelete);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the relations to delete!";
                           request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   relationsToDelete = dbConnectorRelationsToDelete.ObjectRels.Select(rel => new clsObjectRel
                   {
                       ID_Object = rel.ID_Object,
                       ID_RelationType = rel.ID_RelationType,
                       ID_Other = rel.ID_Other
                   }).ToList();

                   relationsToDelete = (from rel in relationsToDelete
                                        join relJoin in relatedJoins on new { rel.ID_RelationType, rel.ID_Parent_Other } equals new { ID_RelationType = relJoin.OItem3.RefItem.GUID, ID_Parent_Other = relJoin.OItem2.RefItem.GUID }
                                        select rel).ToList();

                   if (relationsToDelete.Any())
                   {
                       request.MessageOutput?.OutputInfo($"Delete {relationsToDelete.Count} relations...");
                       result.ResultState = dbConnectorRelationsToDelete.DelObjectRels(relationsToDelete);
                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while deleting the relations!";
                           request.MessageOutput?.OutputError(result.ResultState.Additional1);
                           return result;
                       }
                   }

                   var automaticToDelete = automaticSetsToDelete.Select(rel => new clsObjectRel
                   {
                       ID_Object = rel.ID_Object,
                       ID_RelationType = rel.ID_RelationType,
                       ID_Other = rel.ID_Other
                   }).ToList();

                   result.ResultState = dbConnectorRelationsToDelete.DelObjectRels(automaticToDelete);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while deleting the automatic-relations!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
               }

               request.MessageOutput?.OutputInfo("Have relations to delete.");

               var dbWriterRelations = new OntologyModDBConnector(Globals);
               if (relationsToSet.Any())
               {
                   result.ResultState = dbWriterRelations.SaveObjRel(relationsToSet);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the relations!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
               }

               var relationsToAutomatic = relationsToSet.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Object,
                   ID_Parent_Object = rel.ID_Parent_Object,
                   ID_RelationType = ConvertRelations.Config.LocalData.RelationType_Set_Automatic.GUID,
                   ID_Other = rel.ID_Other,
                   ID_Parent_Other = rel.ID_Parent_Other,
                   OrderID = 1,
                   Ontology = rel.Ontology
               }).ToList();

               if (relationsToAutomatic.Any())
               {
                   result.ResultState = dbWriterRelations.SaveObjRel(relationsToAutomatic);
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the automatic set relations!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
               }


               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<AutoCompleteItem>> GetAutoCompletes(List<clsOntologyItem> classes, string text)
        {
            var taskResult = await Task.Run<ResultItem<AutoCompleteItem>>(async () =>
           {
               var resultService = await serviceAgentElastic.GetObjects(classes, text);
               var result = new ResultItem<AutoCompleteItem>
               {
                   ResultState = resultService.ResultState,
                   Result = new AutoCompleteItem
                   {
                       query = text
                   }
               };

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result.suggestions = resultService.Result.Select(obj => new AutoCompleteSubItem
               {
                   value = $"{obj.ObjectItem.Name} ({obj.ClassItem.Name})",
                   data = obj.ObjectItem.GUID
               }).ToList();

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetOItems(List<string> idObjectList)
        {
            var serviceAgent = new ServiceAgentElastic(Globals);
            var oItemsResult = new ResultItem<List<clsOntologyItem>>();
            if (!idObjectList.Any())
            {
                return oItemsResult;
            }
            oItemsResult = await serviceAgent.GetObjects(idObjectList);

            return oItemsResult;
        }

        public async Task<ResultItem<List<OntoTemplate>>> GetOntoTemplates(Ontology ontology, clsOntologyItem refItem)
        {
            var taskResult = await Task.Run<ResultItem<List<OntoTemplate>>>(() =>
            {
                var result = new ResultItem<List<OntoTemplate>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<OntoTemplate>()
                };


                result.Result = ontology.OntologyJoins.Select(onto =>
                {
                    var ontoTemplate = new OntoTemplate();
                    if (onto.OItem3 == null)
                    {
                        return ontoTemplate;
                    }

                    if (onto.OItem3.RefItem.Type == Globals.Type_RelationType)
                    {
                        ontoTemplate.FirstItem = onto.OItem3.RefItem;
                    }


                    if (onto.OItem1 != null && onto.OItem1.RefItem.GUID == refItem.GUID_Parent && onto.OItem2 != null)
                    {
                        ontoTemplate.SecondItem = onto.OItem2.RefItem;
                    }
                    else if (onto.OItem2 != null && onto.OItem2.RefItem.GUID == refItem.GUID_Parent && onto.OItem1 != null)
                    {
                        ontoTemplate.SecondItem = onto.OItem1.RefItem;
                    }

                    return ontoTemplate;
                }).Where(ontoTempl => !string.IsNullOrEmpty(ontoTempl.FirstName) && !string.IsNullOrEmpty(ontoTempl.SecondName)).OrderBy(ontoTempl => ontoTempl.SecondName).ThenBy(ontoTempl => ontoTempl.SecondName).ToList();

                return result;
            });

            return taskResult;

        }


        public async Task<ResultItem<PossibleRelations>> GetPossibleObjectRelations(string idReference, string baseUrl, string moduleStarterUrl)
        {

            var result = new ResultItem<PossibleRelations>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new PossibleRelations()
            };

            var reference = await GetOItem(idReference, Globals.Type_Object);


            var resultGetTags = await GetTags(reference.Result.GUID, baseUrl, moduleStarterUrl);

            if (resultGetTags.Result.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultGetTags.Result;
                return result;
            }

            var tags = resultGetTags.References.SelectMany(refItm => refItm.Tags).ToList();
            var searchClassRelations = tags.Select(tag => new clsClassRel
            {
                ID_Class_Left = reference.Result.GUID_Parent,
                ID_Class_Right = tag.IdTagParent
            }).ToList();

            searchClassRelations.AddRange(tags.Select(tag => new clsClassRel
            {
                ID_Class_Right = reference.Result.GUID_Parent,
                ID_Class_Left = tag.IdTagParent
            }));

            searchClassRelations.AddRange((from tag1 in tags.Where(tag => !string.IsNullOrEmpty(tag.IdTagParent))
                                           from tag2 in tags.Where(tag => !string.IsNullOrEmpty(tag.IdTagParent))
                                           select new
                                           {
                                               ID_Class_Left = tag1.IdTagParent,
                                               ID_Class_Right = tag2.IdTagParent
                                           }).GroupBy(rel => new { rel.ID_Class_Left, rel.ID_Class_Right }).
                                         Select(relGrp => new clsClassRel
                                         {
                                             ID_Class_Left = relGrp.Key.ID_Class_Left,
                                             ID_Class_Right = relGrp.Key.ID_Class_Right
                                         }));

            var resultClassRel = await serviceAgentElastic.GetClassRelations(searchClassRelations);

            if (resultClassRel.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultClassRel.ResultState;
                return result;
            }

            var searchObjectRelations = (from objLeft in tags
                                         from classRel in resultClassRel.Result
                                         from objRight in tags
                                         where objLeft.IdTagParent == classRel.ID_Class_Left
                                         && classRel.ID_Class_Right == objRight.IdTagParent
                                         //&& objLeft.IdTag != objRight.IdTag
                                         select new
                                         {
                                             ID_Object = objLeft.IdTag,
                                             Name_Object = objLeft.NameTag,
                                             ID_Parent_Object = objLeft.IdTagParent,
                                             Name_Parent_Object = classRel.Name_Class_Left,
                                             ID_RelationType = classRel.ID_RelationType,
                                             ID_Other = objRight.IdTag,
                                             Name_Other = objRight.NameTag,
                                             ID_Parent_Other = objRight.IdTagParent,
                                             Name_Parent_Other = classRel.Name_Class_Right,
                                             Ontology = objRight.TagType
                                         }).GroupBy(relGrp => new
                                         {
                                             ID_Object = relGrp.ID_Object,
                                             Name_Object = relGrp.Name_Object,
                                             ID_Parent_Object = relGrp.ID_Parent_Object,
                                             Name_Parent_Object = relGrp.Name_Parent_Object,
                                             ID_RelationType = relGrp.ID_RelationType,
                                             ID_Other = relGrp.ID_Other,
                                             Name_Other = relGrp.Name_Other,
                                             ID_Parent_Other = relGrp.ID_Parent_Other,
                                             Name_Parent_Other = relGrp.Name_Parent_Other,
                                             Ontology = relGrp.Ontology
                                         }).Select(rel => new clsObjectRel
                                         {
                                             ID_Object = rel.Key.ID_Object,
                                             Name_Object = rel.Key.Name_Object,
                                             ID_Parent_Object = rel.Key.ID_Parent_Object,
                                             Name_Parent_Object = rel.Key.Name_Parent_Object,
                                             ID_RelationType = rel.Key.ID_RelationType,
                                             ID_Other = rel.Key.ID_Other,
                                             Name_Other = rel.Key.Name_Other,
                                             ID_Parent_Other = rel.Key.ID_Parent_Other,
                                             Name_Parent_Other = rel.Key.Name_Parent_Other,
                                             Ontology = rel.Key.Ontology
                                         }).ToList();

            var resultObjectRels = await serviceAgentElastic.GetObjectRelations(searchObjectRelations);

            if (resultObjectRels.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultObjectRels.ResultState;
                return result;
            }


            var factory = new GraphNodeFactory();

            var possbileRelations = (from searchRel in searchObjectRelations
                                     join objRel in resultObjectRels.Result on new { searchRel.ID_Object, searchRel.ID_RelationType, searchRel.ID_Other } equals new { objRel.ID_Object, objRel.ID_RelationType, objRel.ID_Other } into objRels
                                     from objRel in objRels.DefaultIfEmpty()
                                     where objRel == null
                                     select searchRel).ToList();

            result = await factory.CreateGraphNodeList(reference.Result, resultGetTags.References, resultClassRel.Result, resultObjectRels.Result, possbileRelations, Globals);

            return result;
        }

        public async Task<ResultItem<List<GraphNode>>> GetTypedTagRelationsOmni(string idReference, string baseUrl, string moduleStarterUrl)
        {

            var result = new ResultItem<List<GraphNode>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<GraphNode>()
            };

            var reference = await GetOItem(idReference, Globals.Type_Object);


            var resultGetTags = await GetTags(reference.Result.GUID, baseUrl, moduleStarterUrl, TagsRelationType.Omni);

            if (resultGetTags.Result.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultGetTags.Result;
                return result;
            }


            var factory = new GraphNodeFactory();

            result = await factory.CreateGraphNodeList(reference.Result, resultGetTags.References, Globals);

            return result;
        }

        public enum TagsRelationType
        {
            TaggingSource = 0,
            Tag = 1,
            Omni = 2
        }
        public async Task<ConnectorResultTags> GetTags(string idReference, string baseUrl, string moduleStarterUrl, TagsRelationType relationType = TagsRelationType.TaggingSource)
        {
            var oItemReference = serviceAgentElastic.GetOItem(idReference, Globals.Type_Object);

            var result = new ConnectorResultTags
            {
                Result = Globals.LState_Success.Clone()
            };


            if (oItemReference == null)
            {
                result.Result = Globals.LState_Error.Clone();
                return result;
            }

            var taggingSourceResult = new ConnectorResultTypedTags();
            var tagsResult = new ConnectorResultTypedTags();

            var tags = new List<TypedTag>();
            if (relationType == TagsRelationType.TaggingSource ||
                relationType == TagsRelationType.Omni)
            {
                taggingSourceResult = await GetTags(oItemReference);
                if (taggingSourceResult.Result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                tags.AddRange(taggingSourceResult.TypedTags);
            }
            if (relationType == TagsRelationType.Tag ||
                relationType == TagsRelationType.Omni)
            {
                tagsResult = await GetTags(oItemReference, false);
                if (tagsResult.Result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                tags.AddRange(tagsResult.TypedTags);
            }

            var refItemsResult = await GetOItems(tags.Select(tag => tag.IdTag).ToList());

            result.Result = refItemsResult.ResultState;

            if (result.Result.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var tagsWithReferences = (from tag in tags
                                      join refItem in refItemsResult.Result on tag.IdTag equals refItem.GUID into refItems1
                                      from refItem in refItems1.DefaultIfEmpty()
                                      orderby tag.OrderId, tag.NameTag
                                      select new TypedTagWithHtmlPage(tag, refItem != null ? baseUrl + "?Object=" + refItem.GUID : "", moduleStarterUrl));

            var tagCategories = tagsWithReferences.GroupBy(tag => new { IdTagParent = tag.IdTagParent, NameTagParent = tag.NameTagParent }).OrderBy(tag => tag.Key.NameTagParent);
            var references = new List<Reference>();

            foreach (var tagCategory in tagCategories)
            {
                var tagItems = tagsWithReferences.Where(tag => tag.IdTagParent == tagCategory.Key.IdTagParent);
                references.Add(new Reference
                {
                    Id = tagCategory.Key.IdTagParent,
                    Name = tagCategory.Key.NameTagParent,
                    Tags = tagItems.ToList()
                });
            }

            result.References = references;

            return result;
        }

        public async Task<ConnectorResultTypedTags> GetTags(List<clsOntologyItem> oItemTagOrTaggingSources, bool isTaggingSource = true)
        {
            var result = new ConnectorResultTypedTags();

            result.Result = Globals.LState_Success.Clone();

            if (isTaggingSource)
            {
                var typedTagsTask = await serviceAgentElastic.GetTypedTagsByTaggingSources(oItemTagOrTaggingSources);

                result.Result = typedTagsTask.Result;

                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    ResultFoundTypedTags = result;
                    return ResultFoundTypedTags;
                }
                var typedTagsFactoryTask = await typedTaggingFactory.CreateTypedTagsList(typedTagsTask.TypedTagsToTaggingSources, typedTagsTask.TypedTagsToTags);

                result.Result = typedTagsFactoryTask.Result;

                result.TypedTags = typedTagsFactoryTask.TypedTags;
            }
            else
            {
                var typedTagsTask = await serviceAgentElastic.GetTypedTagsByTags(oItemTagOrTaggingSources);

                result.Result = typedTagsTask.Result;

                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    ResultFoundTypedTags = result;
                    return ResultFoundTypedTags;
                }
                var typedTagsFactoryTask = await typedTaggingFactory.CreateTypedTagsList(typedTagsTask.TypedTagsToTaggingSources, typedTagsTask.TypedTagsToTags);

                result.Result = typedTagsFactoryTask.Result;

                result.TypedTags = typedTagsFactoryTask.TypedTags;
            }




            ResultFoundTypedTags = result;
            return ResultFoundTypedTags;
        }

        public async Task<ConnectorResultTypedTags> GetTags(clsOntologyItem oItemTagOrTaggingSource, bool isTaggingSource = true)
        {
            var result = new ConnectorResultTypedTags();

            result.Result = Globals.LState_Success.Clone();

            if (isTaggingSource)
            {
                var typedTagsTask = await serviceAgentElastic.GetTypedTagsByTaggingSources(new List<clsOntologyItem> { oItemTagOrTaggingSource });

                result.Result = typedTagsTask.Result;

                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    ResultFoundTypedTags = result;
                    return ResultFoundTypedTags;
                }
                var typedTagsFactoryTask = await typedTaggingFactory.CreateTypedTagsList(typedTagsTask.TypedTagsToTaggingSources, typedTagsTask.TypedTagsToTags);

                result.Result = typedTagsFactoryTask.Result;

                result.TypedTags = typedTagsFactoryTask.TypedTags;
            }
            else
            {
                var typedTagsTask = await serviceAgentElastic.GetTypedTagsByTags(new List<clsOntologyItem> { oItemTagOrTaggingSource });

                result.Result = typedTagsTask.Result;

                if (result.Result.GUID == Globals.LState_Error.GUID)
                {
                    ResultFoundTypedTags = result;
                    return ResultFoundTypedTags;
                }
                var typedTagsFactoryTask = await typedTaggingFactory.CreateTypedTagsList(typedTagsTask.TypedTagsToTaggingSources, typedTagsTask.TypedTagsToTags);

                result.Result = typedTagsFactoryTask.Result;

                result.TypedTags = typedTagsFactoryTask.TypedTags;
            }

            ResultFoundTypedTags = result;
            return ResultFoundTypedTags;
        }

        public async Task<clsOntologyItem> SaveObjectItems(List<clsOntologyItem> objectItems)
        {
            return await serviceAgentElastic.SaveNewObjectItems(objectItems);
        }

        public async Task<ConnectorResultRelations> SaveRelations(clsOntologyItem oItemRef, List<clsOntologyItem> relItems, clsOntologyItem relationType, clsOntologyItem oItemUser, clsOntologyItem oItemGroup)
        {
            var connectorResult = new ConnectorResultRelations
            {
                Result = Globals.LState_Nothing.Clone(),
                RefItem = oItemRef,
                RelationType = relationType
            };

            var serviceResult = await serviceAgentElastic.SaveRelations(oItemRef, relItems, relationType, oItemUser, oItemGroup);

            connectorResult.Result = serviceResult.ResultState;

            connectorResult.Relations = serviceResult.Relations;

            return connectorResult;
        }

        public async Task<ResultItem<List<OntoRelation>>> SaveOntoRelations(List<FoundOntologyJoin> ontologyJoins, clsOntologyItem refObject)
        {
            var connectorResult = new ResultItem<List<OntoRelation>>
            {
                ResultState = Globals.LState_Nothing.Clone()
            };

            var serviceResult = await serviceAgentElastic.SaveOntoRelations(ontologyJoins, refObject);

            connectorResult = serviceResult;

            return connectorResult;
        }

        public async Task<ConnectorResultTypedTags> SaveTypedTags(clsOntologyItem oItemTagSource, List<TypedTag> typedTags, bool sendToView)
        {
            var connectorResult = new ConnectorResultTypedTags
            {
                Result = Globals.LState_Nothing.Clone(),
                TaggingSource = oItemTagSource,
                Tags = typedTags.Select(typedTag => new clsOntologyItem { GUID = typedTag.IdTag, Name = typedTag.NameTag, GUID_Parent = typedTag.IdTagParent, Type = Globals.Type_Object }).ToList(),
                TypedTags = typedTags
            };

            var serviceResult = await serviceAgentElastic.SaveTypedTags(oItemTagSource, typedTags);

            connectorResult.Result = serviceResult.Result;

            if (connectorResult.Result.GUID == Globals.LState_Error.GUID)
            {
                return connectorResult;
            }


            if (sendToView)
            {
                ResultSaveTypedTags = connectorResult;
            }
            else
            {

            }

            return connectorResult;
        }

        public async Task<clsOntologyItem> RelateTypedTags(clsOntologyItem oItemTagSource, List<clsOntologyItem> typedTags)
        {
            var result = await serviceAgentElastic.RelateTypedTags(oItemTagSource, typedTags);
            return result;
        }

        public async Task<ConnectorResultTypedTags> SaveTags(clsOntologyItem oItemTagSource, List<clsOntologyItem> tags, clsOntologyItem oItemUser, clsOntologyItem oItemGroup, bool sendToView, IMessageOutput messageOutput)
        {
            var connectorResult = new ConnectorResultTypedTags
            {
                Result = Globals.LState_Nothing.Clone(),
                TaggingSource = oItemTagSource,
                Tags = tags
            };

            var serviceResult = await serviceAgentElastic.SaveTypedTags(oItemTagSource, tags, oItemUser, oItemGroup, messageOutput);

            connectorResult.Result = serviceResult.Result;

            if (connectorResult.Result.GUID == Globals.LState_Error.GUID)
            {
                ResultSaveTypedTags = connectorResult;
                return connectorResult;
            }
            messageOutput?.OutputInfo("Create Typed Tagging List...");
            var factoryResult = await typedTaggingFactory.CreateTypedTagsList(serviceResult.TypedTagsToTaggingSources, serviceResult.TypedTagsToTags);

            var typedTags =
                factoryResult.TypedTags.Where(
                    typedTagItem =>
                        typedTagItem.IdTagSource == oItemTagSource.GUID &&
                        tags.Any(tagItem => tagItem.GUID_Parent == Config.LocalData.Class_Typed_Tag.GUID ? tagItem.GUID == typedTagItem.IdTypedTag : tagItem.GUID == typedTagItem.IdTag)).ToList();

            connectorResult.Result = factoryResult.Result;
            connectorResult.TypedTags = typedTags;

            messageOutput?.OutputInfo("Created Typed Tagging List.");
            if (sendToView)
            {
                ResultSaveTypedTags = connectorResult;
            }
            else
            {

            }

            return connectorResult;
        }

        public async Task<ResultReorderTags> ReorderTypedTags(List<TypedTag> typedTags)
        {
            var resultTask = await serviceAgentElastic.ReOrderTypedTags(typedTags);

            var result = resultTask;
            ResultReorderTypedTag = result;
            return result;
        }

        public async Task<clsOntologyItem> DeleteTag(clsOntologyItem oItemTypedTag)
        {
            var resultTask = await serviceAgentElastic.DeleteTypedTag(oItemTypedTag);

            ResultDeleteTypedTag = resultTask;
            return resultTask;
        }
        //public async Task<ResultItem<AutoRelatorOntologyAnalyze>> ParseString(clsOntologyItem refObject, Ontology ontology, string text)
        //{
        //    var textLower = text.ToLower();
        //    var result = new ResultItem<AutoRelatorOntologyAnalyze>()
        //    {
        //        ResultState = Globals.LState_Success.Clone(),
        //        Result = new AutoRelatorOntologyAnalyze
        //        {
        //            IdAnalyze = Globals.NewGUID,
        //            IdRefItem = refObject.GUID,
        //            NameRefItem = refObject.Name,
        //            AnalyzeDate = DateTime.Now,
        //            AnalyzeText = text
        //        }
        //    };

        //    if (string.IsNullOrEmpty(textLower))
        //    {
        //        result.ResultState = Globals.LState_Nothing.Clone();
        //        return result;
        //    }


        //    var ontologyJoinsRelLeftRight = ontology.OntologyJoins.Where(ontoJoin => ontoJoin.OItem3 != null && 
        //        ontoJoin.OItem3.RefItem != null && 
        //        ontoJoin.OItem3.RefItem.Type == Globals.Type_RelationType &&
        //        textLower.Contains(ontoJoin.OItem3.RefItem.Name.ToLower()) &&
        //        ontoJoin.OItem1.RefItem.GUID == refObject.GUID_Parent);

        //    var ontologyJoinsRelRightLeft = ontology.OntologyJoins.Where(ontoJoin => ontoJoin.OItem3 != null &&
        //        ontoJoin.OItem3.RefItem != null &&
        //        ontoJoin.OItem3.RefItem.Type == Globals.Type_RelationType &&
        //        textLower.Contains(ontoJoin.OItem3.RefItem.Name.ToLower()) &&
        //        ontoJoin.OItem2.RefItem.GUID == refObject.GUID_Parent);

        //    var ontologJoinsAtt = ontology.OntologyJoins.Where(ontoJoin => ontoJoin.OItem2 != null && ontoJoin.OItem2.RefItem != null && ontoJoin.OItem2.RefItem.Type == Globals.Type_AttributeType && text.ToLower().Contains(ontoJoin.OItem2.RefItem.Name.ToLower()));

        //    var parseBase = new ParseBase(localConfig);

        //    var wordResult = await parseBase.FindWords(result.Result.AnalyzeText);
        //    if (wordResult.Result.GUID == Globals.LState_Error.GUID)
        //    {
        //        result.ResultState = wordResult.Result;
        //        return result;
        //    }

        //    var searchObjectsLeftRight = (from ontoJoin in ontologyJoinsRelLeftRight
        //                         from word in wordResult.FoundWords
        //                         select new clsOntologyItem
        //                         {
        //                             GUID_Parent = ontoJoin.OItem2.RefItem.GUID,
        //                             Name = word.Word
        //                         }).ToList();

        //    searchObjectsLeftRight.AddRange(ontologyJoinsRelLeftRight.Select(ontoJoin =>
        //    {
        //        var textToSearch = textLower.Replace(ontoJoin.OItem3.RefItem.Name.ToLower(), "");
        //        var oItem = new clsOntologyItem
        //        {
        //            GUID_Parent = ontoJoin.OItem2.RefItem.GUID,
        //            Name = textToSearch
        //        };
        //        return oItem;
        //    }));

        //    var searchObjectsRightLeft = (from ontoJoin in ontologyJoinsRelRightLeft
        //                                  from word in wordResult.FoundWords
        //                                  select new clsOntologyItem
        //                                  {
        //                                      GUID_Parent = ontoJoin.OItem1.RefItem.GUID,
        //                                      Name = word.Word
        //                                  }).ToList();

        //    searchObjectsRightLeft.AddRange(ontologyJoinsRelRightLeft.Select(ontoJoin =>
        //    {
        //        var textToSearch = textLower.Replace(ontoJoin.OItem3.RefItem.Name.ToLower(), "");
        //        var oItem = new clsOntologyItem
        //        {
        //            GUID_Parent = ontoJoin.OItem1.RefItem.GUID,
        //            Name = textToSearch
        //        };
        //        return oItem;
        //    }));




        //    var dbReaderLeftRight = new OntologyModDBConnector(Globals);
        //    var dbReaderRightLeft = new OntologyModDBConnector(Globals);

        //    result.ResultState = dbReaderLeftRight.GetDataObjects(searchObjectsLeftRight);

        //    var ontologyJoinsFound = (from ontoJoin in ontologyJoinsRelLeftRight
        //                              join objectItem in dbReaderLeftRight.Objects1 on ontoJoin.OItem2.RefItem.GUID equals objectItem.GUID_Parent
        //                              join word in wordResult.FoundWords on objectItem.Name.ToLower() equals word.Word.ToLower()
        //                              select new FoundOntologyJoin
        //                              {
        //                                  OntologyJoin = ontoJoin,
        //                                  TextPart2 = new TextPart(objectItem),
        //                                  RelationWord = word,
        //                                  Exists = true
        //                              }).ToList();

        //    ontologyJoinsFound.AddRange(from ontoJoin in ontologyJoinsRelRightLeft
        //                                join objectItem in dbReaderLeftRight.Objects1 on ontoJoin.OItem1.RefItem.GUID equals objectItem.GUID_Parent
        //                                join word in wordResult.FoundWords on objectItem.Name.ToLower() equals word.Word.ToLower()
        //                                select new FoundOntologyJoin
        //                                {
        //                                    OntologyJoin = ontoJoin,
        //                                    TextPart1 = new TextPart(objectItem),
        //                                    RelationWord = word,
        //                                    Exists = true
        //                                });

        //    var combiWords = new List<dynamic>();

        //    for (var i = 0; i < wordResult.FoundWords.Count - 1; i++)
        //    {
        //        FoundWord word = wordResult.FoundWords[i];
        //        FoundWord wordNext = null;
        //        if (wordResult.FoundWords.Count > i + 1)
        //        {
        //            wordNext = wordResult.FoundWords[i + 1];
        //        }

        //        if (wordNext != null)
        //        {
        //            combiWords.Add(new
        //            {
        //                Text = $"{word.Word} {wordNext.Word}",
        //                CombiWords = new FoundCombination
        //                {
        //                    FirstWord = word,
        //                    SecondWord = wordNext
        //                }
        //            });
        //        }


        //    }

        //    ontologyJoinsFound.AddRange (from ontoJoin in ontologyJoinsRelLeftRight
        //                              join objectItem in dbReaderLeftRight.Objects1 on ontoJoin.OItem2.RefItem.GUID equals objectItem.GUID_Parent
        //                              from word in combiWords 
        //                              where objectItem.Name.ToLower().Contains(word.CombiWords.FirstWord.Word.ToLower()) && objectItem.Name.ToLower().Contains(word.CombiWords.SecondWord.Word.ToLower())
        //                              select new FoundOntologyJoin
        //                              {
        //                                  OntologyJoin = ontoJoin,
        //                                  TextPart2 = new TextPart(objectItem),
        //                                  RelationWord = word,
        //                                  Exists = true
        //                              });

        //    ontologyJoinsFound.AddRange(from ontoJoin in ontologyJoinsRelRightLeft
        //                                join objectItem in dbReaderLeftRight.Objects1 on ontoJoin.OItem1.RefItem.GUID equals objectItem.GUID_Parent
        //                                from word in combiWords
        //                                where objectItem.Name.ToLower().Contains(word.CombiWords.FirstWord.Word.ToLower()) && objectItem.Name.ToLower().Contains(word.CombiWords.SecondWord.Word.ToLower())
        //                                select new FoundOntologyJoin
        //                                {
        //                                    OntologyJoin = ontoJoin,
        //                                    TextPart1 = new TextPart(objectItem),
        //                                    RelationWord = word,
        //                                    Exists = false
        //                                });

        //    if (!ontologyJoinsFound.Any(foundOnto => foundOnto.TextPart1 != null && foundOnto.TextPart1.Text.ToLower() != textLower) &&
        //        !ontologyJoinsFound.Any(foundOnto => foundOnto.TextPart2 != null && foundOnto.TextPart2.Text.ToLower() != textLower))
        //    {
        //        if (ontologyJoinsRelLeftRight.Any())
        //        {
        //            ontologyJoinsFound.AddRange(ontologyJoinsRelLeftRight.Select(join => new FoundOntologyJoin
        //            {
        //                OntologyJoin = join,
        //                TextPart2 = new TextPart(new clsOntologyItem
        //                {
        //                    GUID = Globals.NewGUID,

        //                })
        //            }))
        //        }
        //    }

        //    result.Result.OntologyJoins = ontologyJoinsFound;

        //    return result;
        //}

        public async Task<ResultItem<AutoRelatorOntologyAnalyze>> ParseString(clsOntologyItem refObject, Ontology ontology, string text)
        {
            var result = new ResultItem<AutoRelatorOntologyAnalyze>()
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new AutoRelatorOntologyAnalyze
                {
                    IdAnalyze = Globals.NewGUID,
                    IdRefItem = refObject.GUID,
                    NameRefItem = refObject.Name,
                    AnalyzeDate = DateTime.Now,
                    AnalyzeText = text
                }
            };

            if (string.IsNullOrEmpty(result.Result.AnalyzeText))
            {
                result.ResultState = Globals.LState_Nothing.Clone();
                return result;
            }


            var parseBase = new ParseBase(Globals);

            // Get the regex-items the objects and get the classes for the objects
            var regexToClassesResult = await serviceAgentElastic.GetRegexToClasses();

            if (regexToClassesResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = regexToClassesResult.ResultState;
                return result;
            }

            var factory = new RegexItemsToRegexCoClassesFactory();

            // Regex: Create List of OItem-Lists for the application
            var regexToClasses = await factory.CreateList(regexToClassesResult.Result, Globals);

            if (regexToClasses.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = regexToClassesResult.ResultState;
                return result;
            }

            regexToClasses.Result.ForEach(regexToCls => regexToCls.FindMatches(result.Result.AnalyzeText));

            var findRegexObjects = regexToClasses.Result.Where(rgx => ontology.RefItems.Any(refItem => refItem.GUID == rgx.IdClass) && rgx.LastMatches.Any());

            var objectSearchesPre = findRegexObjects.Select(rgx => new
            {
                objects = rgx.LastMatches.Select(match =>
                new clsOntologyItem
                {
                    Name = match.Value,
                    GUID_Parent = rgx.IdClass,
                    OList_Rel = new List<clsOntologyItem> { new clsOntologyItem
                            {
                                GUID = rgx.IdClass,
                                Name = rgx.NameClass,
                                GUID_Parent = rgx.IdClassParent,
                                Type = Globals.Type_Class
                            }
                    }
                })
            }).SelectMany(obj => obj.objects).ToList();

            var objectsFound = await serviceAgentElastic.GetObjectClassesByName(objectSearchesPre);

            var wordResult = await parseBase.FindWords(result.Result.AnalyzeText);
            if (wordResult.Result.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = wordResult.Result;
                return result;
            }

            var ordered = new List<TextPart>();

            foreach (var regexToClass in findRegexObjects)
            {
                foreach (var item in regexToClass.LastMatches)
                {
                    var objectsFoundByRegex = objectsFound.Result.Where(obj => obj.ClassItem.GUID == regexToClass.IdClass && obj.ObjectItem.Name.ToLower() == item.Value.ToLower()).ToList();

                    var foundRegex = objectsFoundByRegex.Select(obj => new TextPart(obj.ObjectItem)
                    {
                        IdObject = obj.ObjectItem.GUID,
                        IdClass = obj.ClassItem.GUID,
                        NameClass = obj.ClassItem.Name,
                        Text = obj.ObjectItem.Name,
                        IxStart = item.Index,
                        IxEnd = item.Index + item.Length
                    }).ToList();

                    if (!foundRegex.Any())
                    {
                        var objectItem = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = item.Value.Length > 255 ? item.Value.Substring(0, 254) : item.Value,
                            GUID_Parent = regexToClass.IdClass,
                            Type = Globals.Type_Object,
                            New_Item = true
                        };
                        foundRegex.Add(new TextPart(objectItem)
                        {
                            IdObject = objectItem.GUID,
                            IdClass = regexToClass.IdClass,
                            NameClass = regexToClass.NameClass,
                            Text = objectItem.Name,
                            IxStart = item.Index,
                            IxEnd = item.Index + item.Length
                        });
                    }

                    ordered.AddRange(foundRegex);
                }
            }
            var classes = ontology.RefItems.Where(refItm => refItm.Type == Globals.Type_Class).GroupBy(cls => cls.GUID).Select(cls => new clsOntologyItem { GUID = cls.Key }).ToList();

            var searchResult = await serviceAgentElastic.GetObjectsOfClasses(classes, wordResult.FoundWords);

            if (searchResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = searchResult.ResultState;
                return result;
            }

            var lowerText = result.Result.AnalyzeText.ToLower();
            var foundObjectClasses = searchResult.Result.Where(objCls => lowerText.Contains(objCls.ObjectItem.Name.ToLower())).ToList();

            ordered.AddRange((from foundTextPart in foundObjectClasses.Select(objCls =>
            {
                var startIx = lowerText.IndexOf(objCls.ObjectItem.Name.ToLower());
                var endIx = startIx + objCls.ObjectItem.Name.Length;
                var textPartStr = result.Result.AnalyzeText.Substring(startIx, endIx - startIx);
                var textPart = new TextPart(objCls.ObjectItem)
                {
                    IxStart = startIx,
                    IxEnd = endIx,
                    IdObject = objCls.ObjectItem.GUID,
                    IdClass = objCls.ClassItem.GUID,
                    NameClass = objCls.ClassItem.Name,
                    Text = textPartStr
                };

                return textPart;
            })
                              join orderedExist in ordered on new { foundTextPart.IdObject, foundTextPart.IxStart, foundTextPart.IxEnd } equals new { orderedExist.IdObject, orderedExist.IxStart, orderedExist.IxEnd } into orderedExists
                              from orderedExist in orderedExists.DefaultIfEmpty()
                              where orderedExist == null
                              select foundTextPart));

            result.Result.TextParts = ordered.Where(order => order.IdObject != refObject.GUID).OrderBy(order => order.IxStart).ToList();


            ontology.OntologyJoins.Where(oJoin => oJoin.OItem1 != null && oJoin.OItem2 != null && oJoin.OItem3 != null).ToList().ForEach(oJoin =>
            {

                var oItems1Found = result.Result.TextParts.Where(textPart => textPart.IdClass == oJoin.OItem1.RefItem.GUID).ToList();
                var oItems2Found = result.Result.TextParts.Where(textPart => textPart.IdClass == oJoin.OItem2.RefItem.GUID).ToList();
                var oItems3Found = wordResult.FoundWords.Where(word => word.Word.ToLower() == oJoin.OItem3.RefItem.Name).ToList();

                var oItemsFound = new List<TextPart>();


                var oJoinsFoundLeftRight = (from oItem2 in oItems2Found
                                            from oItem3 in oItems3Found
                                            where oItem3.StartIx < oItem2.IxStart
                                            select new { oItem2, oItem3 });

                var oJoinsFoundRightLeft = (from oItem1 in oItems1Found
                                            from oItem3 in oItems3Found
                                            where oItem3.StartIx < oItem1.IxStart
                                            select new { oItem1, oItem3 });


                foreach (var item in oJoinsFoundLeftRight)
                {
                    oItemsFound.Add(item.oItem2);

                    result.Result.OntologyJoins.Add(new FoundOntologyJoin
                    {
                        OntologyJoin = oJoin,
                        TextPart2 = item.oItem2,
                        RelationWord = item.oItem3
                    });
                }

                foreach (var item in oJoinsFoundRightLeft)
                {
                    oItemsFound.Add(item.oItem1);
                    result.Result.OntologyJoins.Add(new FoundOntologyJoin
                    {
                        OntologyJoin = oJoin,
                        TextPart1 = item.oItem1,
                        RelationWord = item.oItem3
                    });
                }

                oItems1Found = (from oItm in oItems1Found
                                join oItmExist in oItemsFound on oItm equals oItmExist into oItmExists
                                from oItmExist in oItmExists.DefaultIfEmpty()
                                where oItmExist == null
                                select oItm).ToList();

                oItems2Found = (from oItm in oItems2Found
                                join oItmExist in oItemsFound on oItm equals oItmExist into oItmExists
                                from oItmExist in oItmExists.DefaultIfEmpty()
                                where oItmExist == null
                                select oItm).ToList();

                if (oJoin.OItem3 != null)
                {
                    if (!text.ToLower().Contains(oJoin.OItem3.RefItem.Name.ToLower())) return;
                }

                foreach (var item in oItems1Found)
                {
                    result.Result.OntologyJoins.Add(new FoundOntologyJoin
                    {
                        OntologyJoin = oJoin,
                        TextPart1 = item
                    });
                }

                foreach (var item in oItems2Found)
                {
                    result.Result.OntologyJoins.Add(new FoundOntologyJoin
                    {
                        OntologyJoin = oJoin,
                        TextPart2 = item
                    });
                }

            });

            var textLower = text.ToLower();
            var relationsLeftRight = result.Result.OntologyJoins.Where(oJoin => oJoin.OntologyJoin.OItem3 != null &&
                oJoin.OntologyJoin.OItem3.RefItem.Type == Globals.Type_RelationType &&
                textLower.Contains(oJoin.OntologyJoin.OItem3.RefItem.Name.ToLower()) &&
                oJoin.TextPart2 != null &&
                oJoin.OntologyJoin.OItem1.RefItem.GUID == refObject.GUID_Parent);

            var searchRelationsLeftRight = relationsLeftRight
                .Select(oJoin => new clsObjectRel
                {
                    ID_Object = refObject.GUID,
                    ID_RelationType = oJoin.OntologyJoin.OItem3.RefItem.GUID,
                    ID_Other = oJoin.TextPart2.IdObject
                }).ToList();


            var dbReaderRelations = new OntologyModDBConnector(Globals);

            if (searchRelationsLeftRight.Any())
            {
                result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelationsLeftRight);

                if (searchResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

            }


            (from relationLeftRight in relationsLeftRight
             join relItem in dbReaderRelations.ObjectRels on new
             {
                 IdRelationType = relationLeftRight.OntologyJoin.OItem3.RefItem.GUID,
                 IdOther = relationLeftRight.TextPart2.IdObject
             } equals new
             {
                 IdRelationType = relItem.ID_RelationType,
                 IdOther = relItem.ID_Other
             }
             select relationLeftRight).ToList().ForEach(relation =>
             {
                 relation.Exists = true;
             });

            var relationsRightLeft = result.Result.OntologyJoins.Where(oJoin => oJoin.OntologyJoin.OItem3 != null &&
                oJoin.OntologyJoin.OItem3.RefItem.Type == Globals.Type_RelationType &&
                textLower.Contains(oJoin.OntologyJoin.OItem3.RefItem.Name.ToLower()) &&
                oJoin.TextPart1 != null &&
                oJoin.OntologyJoin.OItem2.RefItem.GUID == refObject.GUID_Parent);

            var searchRelationsRightLeft = relationsRightLeft
                .Select(oJoin => new clsObjectRel
                {
                    ID_Other = refObject.GUID,
                    ID_RelationType = oJoin.OntologyJoin.OItem3.RefItem.GUID,
                    ID_Object = oJoin.TextPart1.IdObject
                }).ToList();


            if (searchRelationsRightLeft.Any())
            {
                result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelationsRightLeft);

                if (searchResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }


            (from relationRightLeft in relationsRightLeft
             join relItem in dbReaderRelations.ObjectRels on new
             {
                 IdRelationType = relationRightLeft.OntologyJoin.OItem3.RefItem.GUID,
                 IdObject = relationRightLeft.TextPart1.IdObject
             } equals new
             {
                 IdRelationType = relItem.ID_RelationType,
                 IdObject = relItem.ID_Object
             }
             select relationRightLeft).ToList().ForEach(relation =>
             {
                 relation.Exists = true;
             });


            return result;
        }

        public async Task<ResultItem<AutoRelatorAnalyze>> ParseString(clsOntologyItem refObject, bool searchName, string text, string idClassForSearch)
        {
            var result = new ResultItem<AutoRelatorAnalyze>()
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new AutoRelatorAnalyze
                {
                    IdAnalyze = Globals.NewGUID,
                    IdRefItem = refObject.GUID,
                    NameRefItem = refObject.Name,
                    AnalyzeDate = DateTime.Now,
                    AnalyzeText = text
                }
            };

            if (string.IsNullOrEmpty(result.Result.AnalyzeText))
            {
                result.ResultState = Globals.LState_Nothing.Clone();
                return result;
            }
            result.Result.FoundTexts = new List<FoundText>();

            var parseBase = new ParseBase(Globals);

            // Get the regex-items the objects and get the classes for the objects
            var regexToClassesResult = await serviceAgentElastic.GetRegexToClasses();

            if (regexToClassesResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = regexToClassesResult.ResultState;
                return result;
            }

            var factory = new RegexItemsToRegexCoClassesFactory();

            // Regex: Create List of OItem-Lists for the application
            var regexToClasses = await factory.CreateList(regexToClassesResult.Result, Globals);

            if (regexToClasses.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = regexToClassesResult.ResultState;
                return result;
            }

            result.Result.FoundTerms = new List<FoundTerm>();

            // Get the Words from the analyzed text
            var wordResult = await parseBase.FindWords(result.Result.AnalyzeText);
            if (wordResult.Result.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = wordResult.Result;
                return result;
            }

            var guids = wordResult.FoundWords.Where(word => Globals.is_GUID(word.Word)).Select(word => word).ToList();
            //var nonGuids = wordResult.FoundWords.Where(word => !Globals.is_GUID(word.Word)).Select(word => word);
            //var validWords = (from word in nonGuids.Where(word => word.Word.Length >= 3)
            //                  join fillWord in ParseParts.FillWords on word.Word.ToLower() equals fillWord.ToLower() into fillWords
            //                  from fillWord in fillWords.DefaultIfEmpty()
            //                  where fillWord == null
            //                  select word).ToList();
            var objectClassList = new List<FoundTerm>();


            // search the Objects with classes of analyzed words
            //var textToAnalyze = string.Join(" ", validWords.Select(validWord => validWord.Word));

            //var searchObjectResult = await serviceAgentElastic.SearchObjects(validWords, guids);

            var textWithObjects = "";
            if (searchName)
            {
                textWithObjects = text;
            }
            else
            {
                textWithObjects = string.Join(" ", wordResult.FoundWords.Select(word => word.Word));
            }
            //var searchObjectResult = await serviceAgentElastic.SearchObjects(wordResult.FoundWords, guids);
            var searchObjectResult = await serviceAgentElastic.SearchObjects(textWithObjects, guids, idClassForSearch);
            if (searchObjectResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = searchObjectResult.ResultState;
                return result;
            }

            objectClassList = searchObjectResult.Result;
            result.Result.FoundTerms = searchObjectResult.Result.Where(obj => obj.ObjectItem.GUID != refObject.GUID).ToList();

            //var searchNameCombinations = new List<clsOntologyItem>();

            //result.Result.FoundCombinations = new List<FoundCombination>();
            //for (var i = 0; i < wordResult.FoundWords.Count; i++)
            //{
            //    if (i + 1 < wordResult.FoundWords.Count)
            //    {
            //        var word = wordResult.FoundWords[i];
            //        var nextWord = wordResult.FoundWords[i + 1];

            //        var rangeStart = word.StartIx;
            //        var rangeEnd = nextWord.EndIx;

            //        var combinationText = result.Result.AnalyzeText.Substring(rangeStart, rangeEnd - rangeStart + 1);


            //        result.Result.FoundCombinations.Add(new FoundCombination
            //        {
            //            FirstWord = word,
            //            SecondWord = nextWord,
            //            Text = combinationText
            //        });


            //    }
            //}


            //if (result.Result.FoundCombinations.Any())
            //{

            //    result.Result.FoundCombinations = (from foundCombination in result.Result.FoundCombinations
            //                                       join objectClass in objectClassList.GroupBy(objCls => new { objCls.ObjectItem, objCls.ClassItem }) on foundCombination.Text.ToLower() equals objectClass.Key.ObjectItem.Name.ToLower()
            //                                       select new FoundCombination
            //                                       {
            //                                           FirstWord = foundCombination.FirstWord,
            //                                           SecondWord = foundCombination.SecondWord,
            //                                           Text = foundCombination.Text,
            //                                           ObjectItem = objectClass.Key.ObjectItem,
            //                                           ClassItem = objectClass.Key.ClassItem
            //                                       }).ToList();
            //}


            var foundObjects = result.Result.FoundTerms.Select(term => new ObjectClass { ObjectItem = term.ObjectItem, ClassItem = term.ClassItem }).ToList();
            //foundObjects.AddRange(result.Result.FoundTexts.Select(textToAnalyze => new ObjectClass { ObjectItem = textToAnalyze.ObjectItem, ClassItem = textToAnalyze.ClassItem }));
            //foundObjects.AddRange(result.Result.FoundCombinations.Select(combi => new ObjectClass { ObjectItem = combi.ObjectItem, ClassItem = combi.ClassItem }));

            result.Result.FoundRegex = new List<FoundRegex>();
            foreach (var regex in regexToClasses.Result)
            {
                var classItem = new clsOntologyItem
                {
                    GUID = regex.IdClass,
                    Name = regex.NameClass,
                    GUID_Parent = regex.IdClassParent,
                    Type = Globals.Type_Class
                };

                var matches = regex.FindMatches(result.Result.AnalyzeText);
                var objectMatches = (from match in matches
                                     join obj in foundObjects on new { Name = match.Value.ToLower(), GUID_Parent = regex.IdClass } equals new { Name = obj.ObjectItem.Name.ToLower(), GUID_Parent = obj.ObjectItem.GUID_Parent } into objs
                                     from obj in objs.DefaultIfEmpty()
                                     select new { match, obj });

                var objectItemsNew = objectMatches.Where(objMatch => objMatch.obj == null).Select(match => new clsOntologyItem { Name = match.match.Value, New_Item = true, GUID_Parent = regex.IdClass, Type = Globals.Type_Object }).ToList();

                if (objectItemsNew.Any())
                {
                    var searchRegexObjectsResult = await serviceAgentElastic.SearchObjects(objectItemsNew);

                    if (searchRegexObjectsResult.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = searchRegexObjectsResult.ResultState;
                        return result;
                    }

                    (from objectItem in objectItemsNew
                     join objectItemFound in searchRegexObjectsResult.Result on objectItem.Name.ToLower() equals objectItemFound.ObjectItem.Name.ToLower() into objectItemFounds
                     from objectItemFound in objectItemFounds.DefaultIfEmpty()
                     join objectMatch in objectMatches on objectItem.Name equals objectMatch.match.Value
                     select new { objectItem, objectItemFound, objectMatch }).ToList().ForEach(objAndFound =>
                      {
                          objAndFound.objectItem.GUID = objAndFound.objectItemFound != null ? objAndFound.objectItemFound.ObjectItem.GUID : Globals.NewGUID;
                          objAndFound.objectItem.Name = objAndFound.objectItemFound != null ? objAndFound.objectItemFound.ObjectItem.Name : objAndFound.objectItem.Name;
                          objAndFound.objectItem.New_Item = objAndFound.objectItemFound == null;
                          result.Result.FoundRegex.Add(new FoundRegex { RegexToClass = regex, ObjectItem = objAndFound.objectItem, ClassItem = classItem, Match = objAndFound.objectMatch.match });
                      });

                }
            }

            var ordered = new List<TextPart>();

            ordered.AddRange(result.Result.FoundTexts.GroupBy(txt => new { txt.ObjectItem, txt.ClassItem }).Select(txt => new TextPart(txt.Key.ObjectItem)
            {
                IxStart = 0,
                IxEnd = result.Result.AnalyzeText.Length - 1,
                IdObject = txt.Key.ObjectItem.GUID,
                IdClass = txt.Key.ClassItem.GUID,
                NameClass = txt.Key.ClassItem.Name,
                Text = txt.Key.ObjectItem.Name
            }));

            //var foundCombinations = (from foundCombination in result.Result.FoundCombinations.Where(combi => combi.ClassItem.GUID != Config.LocalData.Class_Typed_Tag.GUID).Select(combi => new TextPart(combi.ObjectItem)
            //{
            //    IxStart = combi.FirstWord.StartIx,
            //    IxEnd = combi.SecondWord.EndIx,
            //    IdObject = combi.ObjectItem.GUID,
            //    Text = combi.Text,
            //    IdClass = combi.ClassItem.GUID,
            //    NameClass = combi.ClassItem.Name
            //})
            //                         join orderedItem in ordered on new { foundCombination.IdObject, foundCombination.IxStart, foundCombination.IxEnd } equals new { orderedItem.IdObject, orderedItem.IxStart, orderedItem.IxEnd } into orderedItems
            //                         from orderedItem in orderedItems.DefaultIfEmpty()
            //                         where orderedItem == null
            //                         select foundCombination);

            //ordered.AddRange(foundCombinations);


            var foundTerms = (from foundTerm in result.Result.FoundTerms.Where(term => (term.FoundByClass || term.Similarity > 0.5) && term.ClassItem.GUID != Config.LocalData.Class_Typed_Tag.GUID).Select(term => new TextPart(term.ObjectItem)
            {
                IxStart = term.FoundWord.StartIx,
                IxEnd = term.FoundWord.EndIx,
                IdObject = term.ObjectItem.GUID,
                Text = term.ObjectItem.Name,
                IdClass = term.ClassItem.GUID,
                NameClass = term.ClassItem.Name,
                Similarity = term.Similarity,
                FoundByClass = term.FoundByClass,
                FoundInText = term.FoundInText
            })
                              join orderedItem in ordered on new { foundTerm.IdObject, foundTerm.IxStart, foundTerm.IxEnd } equals new { orderedItem.IdObject, orderedItem.IxStart, orderedItem.IxEnd } into orderedItems
                              from orderedItem in orderedItems.DefaultIfEmpty()
                              where orderedItem == null
                              select foundTerm);

            ordered.AddRange(foundTerms);


            var foundRegex = (from regexItem in result.Result.FoundRegex.Where(regx => regx.ClassItem.GUID != Config.LocalData.Class_Typed_Tag.GUID).Select(regx => new TextPart(regx.ObjectItem)
            {
                IxStart = regx.Match.Index,
                IxEnd = regx.Match.Index + regx.Match.Length,
                IdObject = regx.ObjectItem.GUID,
                Text = regx.Match.Value,
                IdClass = regx.ClassItem.GUID,
                NameClass = regx.ClassItem.Name,
                FoundInText = true,
                FoundByRegex = true
            })
                              join orderedItem in ordered on new { regexItem.IdObject, regexItem.IxStart, regexItem.IxEnd } equals new { orderedItem.IdObject, orderedItem.IxStart, orderedItem.IxEnd } into orderedItems
                              from orderedItem in orderedItems.DefaultIfEmpty()
                              where orderedItem == null
                              select regexItem);

            ordered.AddRange(foundRegex);

            result.Result.TextParts = ordered.Where(order => order.IdObject != refObject.GUID).
                OrderByDescending(order => order.FoundByRegex).
                ThenByDescending(order => order.FoundByClass).
                ThenByDescending(order => order.FoundInText).
                ThenBy(order => order.IxStart).ThenByDescending(order => order.Similarity).ToList();

            if (result.Result.TextParts.Count > 100)
            {
                result.Result.TextParts = result.Result.TextParts.GetRange(0, 100);
            }

            result.Result.TextRanges = new List<TextRange>();

            result.Result.TextParts.ForEach(txt =>
            {
                var textRangesNotContaining = result.Result.TextRanges.Where(textRange => !textRange.IsContained(txt.IxStart, txt.IxEnd)).ToList();

                if (!textRangesNotContaining.Any())
                {
                    result.Result.TextRanges.Add(new TextRange
                    {
                        RangeRefItems = new List<TextPart>
                        {
                            txt
                        }
                    });
                }
                else
                {
                    textRangesNotContaining.First().RangeRefItems.Add(txt);
                }

            });

            return result;
        }


        public TypedTaggingConnector(Globals globals) : base(globals)
        {
            Initialize();
        }

        private void Initialize()
        {
            serviceAgentElastic = new ServiceAgentElastic(Globals);
            typedTaggingFactory = new TypedTaggingFactory();
        }
    }

    public class ConnectorResultTypedTags
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem TaggingSource { get; set; }
        public List<clsOntologyItem> Tags { get; set; }
        public List<TypedTag> TypedTags { get; set; }
    }

    public class ConnectorResultRelations
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem RelationType { get; set; }
        public List<clsObjectRel> Relations { get; set; }
    }

    public class OntoRelation
    {
        public FoundOntologyJoin OntologyJoin { get; set; }
        public clsObjectRel Relation { get; set; }
    }

    public class OntoTemplate
    {
        public clsOntologyItem FirstItem { get; set; }
        public clsOntologyItem SecondItem { get; set; }

        public string FirstName
        {
            get
            {
                return FirstItem != null ? FirstItem.Name : "";
            }
        }

        public string SecondName
        {
            get
            {
                return SecondItem != null ? SecondItem.Name : "";
            }
        }

        public string Template
        {
            get
            {
                var template = "";
                if (FirstItem != null)
                {
                    template = FirstItem.Name;
                }

                if (SecondItem != null)
                {
                    template += " " + SecondItem.Name.ToUpper().
                                            Replace(" ", "_").
                                            Replace("(", "_").
                                            Replace(".", "_").
                                            Replace(")", "_").
                                            Replace(";", "_").
                                            Replace(",", "_").
                                            Replace("-", "_");
                }


                return template;
            }
        }
    }
}
