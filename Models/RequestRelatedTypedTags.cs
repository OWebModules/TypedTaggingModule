﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class RequestRelatedTypedTags
    {
        public string IdClassTaggingSource { get; set; }
        public string IdRelationTypeTaggingSourceToRef { get; set; }

        public RequestRelatedTypedTags(string idClassTaggingSource, string idRelationTypeTaggingSourceToRef)
        {
            IdClassTaggingSource = idClassTaggingSource;
            IdRelationTypeTaggingSourceToRef = idRelationTypeTaggingSourceToRef;
        }
    }
}
