﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public static class ParseParts
    {
        public static string PreClassIdentPattern => "(Klasse|klasse)";
        public static string Brackets => "^\\(.*\\)$";
        public static string PreObjectIdentPattern => "(Objekt|object)";
        public static string NotString => @"\S";
        public static string WordPattern => $"{NotString}+";
        public static string GUIDPattern => @"([0-9A-Fa-f]{8}[-]?[0-9A-Fa-f]{4}[-]?[0-9A-Fa-f]{4}[-]?[0-9A-Fa-f]{4}[-]?[0-9A-Fa-f]{12})";
        public static string SeperatorsPattern => @"(\s+|,|,\s+|;|;\s+|/|/\s+|:|:\s+|\\|\.|\\\s+)";
        public static string ClassPrefixPattern => "^((?i)(Einem)|(?i)(Einer)|(?i)(Ein)|(?i)(Welche))$";
        public static string PrefixPattern => "^((?i)(Der)|(?i)(Die)|(?i)(Das)|(?i)(Dem)|(?i)(Den)|(?i)(Als))$";
        public static string SubClassPattern => "((?i)(Untergeordnet)|(?i)(Instanz))";
        public static string IdentityVerbs => "((?i)(Ist)|(?i)(Entspricht))";
        public static string PunctuationMarkPattern => @"(\.|;|\!|\?|:|,)";
        public static string LowerCaseStartPattern => @"^[a-z]";
        public static string UpperCaseStartPattern => @"^[A-Z]";
        public static string AimPattern => @"(folgendes|folgend|folgendem)";
        public static string ReasonPattern => @"(damit|um)";
        public static string UpperCase => @"^\(?[A-ZÄÖÜß]+\)?$";

        public static string IntPattern => @"\d+";
        public static string DoublePattern => @"(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?";
        public static string BoolPattern => @"((?i)Wahr|(?i)Falsch)";
        public static string DateTimePattern => @"(\d{2}|\d)\.(\d{2}|\d).(\d{2}|\d{4})(\s(\d{2}:\d{2}:\d{2}))?";

        public static List<string> FillWords => new List<string> { "als", "die", "der", "das", "wie", "wo", "wer", "in", "im", "von", "am", "ob", "an", "des" };
        
        

    }
}
