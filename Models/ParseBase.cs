﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TypedTaggingModule.Services;

namespace TypedTaggingModule.Models
{
    public class ParseBase
    {
        private Globals globals;
        protected clsLogStates logStates = new clsLogStates();
        protected ServiceAgentElastic serviceAgent;
        protected clsRelationConfig relationConfig;

        protected void CheckNomen(FoundWord foundWord, string sentense)
        {
            var wordToTest = foundWord.Word.Trim();
            if (foundWord.StartIx == 0) return;

            if (Regex.IsMatch(wordToTest, ParseParts.UpperCaseStartPattern))
            {
                var character = "";
                var startIx = foundWord.StartIx - 1;
                for (int ix = startIx; ix >= 0; ix--)
                {
                    character = sentense.Substring(ix, 1);
                    if (Regex.IsMatch(character, ParseParts.NotString)) break;
                }

                if (Regex.IsMatch(character, ParseParts.PunctuationMarkPattern)) return;

                foundWord.Nomen = true;
            }

        }

        protected void ParseQuotationWords(string sentense, List<FoundWord> words)
        {
            var regexQuotationMarks = new Regex("\"");
            var matchesQuotationMarks = regexQuotationMarks.Matches(sentense);
            for (int ix = 0; ix < matchesQuotationMarks.Count; ix += 2)
            {
                if (matchesQuotationMarks.Count - 1 > ix)
                {
                    var start = matchesQuotationMarks[ix].Index + 1;
                    var end = matchesQuotationMarks[ix + 1].Index;
                    if (end > start + 1)
                    {
                        var wordText = sentense.Substring(start, end - start);
                        var word = new FoundWord
                        {
                            Word = wordText,
                            StartIx = start - 1,
                            EndIx = end,
                            Quoted = true,
                            OItemWord = new clsOntologyItem
                            {
                                Name = wordText,
                                GUID_Parent = Config.LocalData.Class_Word.GUID,
                                Type = globals.Type_Object,
                                Val_Long = start - 1
                            }
                        };
                        CheckNomen(word, sentense);
                        words.Add(word);
                    }
                }
            }

        }

        protected void ParseSeperators(string sentense, List<FoundWord> words)
        {
            var regexSeperators = new Regex(ParseParts.SeperatorsPattern);
            var matchesSeperators = regexSeperators.Matches(sentense);

            for (int ix = 0; ix < matchesSeperators.Count; ix++)
            {
                if (ix == 0)
                {
                    if (!words.Any(word => matchesSeperators[ix].Index >= word.StartIx && matchesSeperators[ix].Index <= word.EndIx))
                    {
                        var word = sentense.Substring(0, matchesSeperators[ix].Index);

                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = 0,
                                EndIx = matchesSeperators[ix].Index,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = Config.LocalData.Class_Word.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = 0
                                }
                            };
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }
                    }
                    if (matchesSeperators.Count == 1)
                    {
                        var word = sentense.Substring(matchesSeperators[ix].Index + matchesSeperators[ix].Length);
                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = matchesSeperators[ix].Index + matchesSeperators[ix].Length,
                                EndIx = sentense.Length-1,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = Config.LocalData.Class_Word.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = 0
                                }
                            };
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }
                    }
                }
                else if (ix == matchesSeperators.Count - 1)
                {
                    var matchBefore = matchesSeperators[ix - 1];
                    var match = matchesSeperators[ix];

                    if (!words.Any(word => matchBefore.Index + matchBefore.Length + 1 >= word.StartIx - 1
                         && matchBefore.Index + matchBefore.Length + 1 <= word.EndIx))
                    {
                        var word = sentense.Substring(matchBefore.Index + matchBefore.Length, match.Index - (matchBefore.Index + matchBefore.Length));

                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = matchBefore.Index + matchBefore.Length,
                                EndIx = matchBefore.Index + matchBefore.Length + word.Length,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = Config.LocalData.Class_Word.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = matchBefore.Index + matchBefore.Length
                                }
                            };
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }

                    }

                    if (!words.Any(word => matchesSeperators[ix].Index + matchesSeperators[ix].Length + 1 >= word.StartIx - 1
                         && matchesSeperators[ix].Index + matchesSeperators[ix].Length + 1 <= word.EndIx))
                    {
                        var word = sentense.Substring(matchesSeperators[ix].Index + matchesSeperators[ix].Length);

                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = matchesSeperators[ix].Index + matchesSeperators[ix].Length,
                                EndIx = matchesSeperators[ix].Index + matchesSeperators[ix].Length + word.Length-1,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = Config.LocalData.Class_Word.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = matchesSeperators[ix].Index + matchesSeperators[ix].Length
                                }
                            };
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }
                    }
                }
                else
                {
                    var matchBefore = matchesSeperators[ix - 1];
                    var match = matchesSeperators[ix];

                    if (!words.Any(word => matchBefore.Index + matchBefore.Length + 1 >= word.StartIx - 1
                         && matchBefore.Index + matchBefore.Length + 1 <= word.EndIx))
                    {
                        var word = sentense.Substring(matchBefore.Index + matchBefore.Length, match.Index - (matchBefore.Index + matchBefore.Length));

                        if (!string.IsNullOrEmpty(word))
                        {
                            var wordToAdd = new FoundWord
                            {
                                Word = word,
                                StartIx = matchBefore.Index + matchBefore.Length,
                                EndIx = matchBefore.Index + matchBefore.Length + word.Length - 1,
                                OItemWord = new clsOntologyItem
                                {
                                    Name = word,
                                    GUID_Parent = Config.LocalData.Class_Word.GUID,
                                    Type = globals.Type_Object,
                                    Val_Long = matchBefore.Index + matchBefore.Length
                                }
                            };
                            CheckNomen(wordToAdd, sentense);
                            words.Add(wordToAdd);
                        }

                    }
                }
            }
        }

        public async Task<ResultFindWords> FindWords(string text)
        {
            var taskResult = await Task.Run<ResultFindWords>(() =>
            {
                var result = new ResultFindWords
                {
                    Result = globals.LState_Success.Clone()
                };

                if (text.Length == 0)
                {
                    result.Result = globals.LState_Nothing.Clone();
                    return result;
                }

                var words = new List<FoundWord>();

                ParseQuotationWords(text, words);
                ParseSeperators(text, words);

                if (!words.Any())
                {
                    words.Add(new FoundWord
                    {
                        StartIx = 0,
                        EndIx = text.Length-1,
                        Brackets = Regex.IsMatch(text, ParseParts.Brackets),
                        Word = (text.Substring(0, 1) + text.Substring(1).ToLower()).Replace(@"(\(|\))", ""),
                        OItemWord = new clsOntologyItem
                        {
                            Name = text.Length > 255 ? text.Substring(0,254) : text,
                            GUID_Parent = Config.LocalData.Class_Word.GUID,
                            Val_Long = 0,
                            Type = globals.Type_Object
                        }
                    });
                }
                words = words.OrderBy(match => match.StartIx).ToList();

                words.Where(word => Regex.IsMatch(word.Word, ParseParts.UpperCase)).ToList().ForEach(word =>
                {
                    word.Brackets = Regex.IsMatch(word.Word, ParseParts.Brackets);
                    word.Word = Regex.Replace(word.Word, @"(\(|\))", "");
                    word.Word = word.Word.Substring(0, 1) + word.Word.Substring(1).ToLower();
                    word.UpperCase = true;
                    word.OItemWord = new clsOntologyItem
                    {
                        Name = word.Word,
                        GUID_Parent = Config.LocalData.Class_Word.GUID,
                        Val_Long = word.StartIx,
                        Type = globals.Type_Object
                    };
                });



                result.FoundWords = words;

                return result;
            });

            return taskResult;
        }

        public ParseBase(Globals globals)
        {
            this.globals = globals;
            this.serviceAgent = new ServiceAgentElastic(globals);
            this.relationConfig = new clsRelationConfig(globals);
        }
    }

    public class ResultParse
    {
        public clsOntologyItem Result { get; set; }
        public clsObjectRel ObjectRel { get; set; }
        public clsClassAtt ClassAtt { get; set; }
        public clsClassRel ClassRel { get; set; }
        public clsObjectAtt ObjectAtt { get; set; }
    }

    public class FoundWord
    {
        public string Word { get; set; }
        public int StartIx { get; set; }
        public int EndIx { get; set; }
        public bool Brackets { get; set; }
        public bool Quoted { get; set; }
        public bool Nomen { get; set; }
        public bool UpperCase { get; set; }
        public clsOntologyItem OItemWord { get; set; }
    }

    

    public class ResultFindWords
    {
        public clsOntologyItem Result { get; set; }
        public List<FoundWord> FoundWords { get; set; }

    }



    public class FoundClass
    {
        public FoundWord FoundWord { get; set; }
        public clsOntologyItem ClassItem { get; set; }
        public Match GuidMatch { get; set; }
        public bool Pre { get; set; }
    }

    public class FoundClasses
    {
        public List<FoundClass> ClassesFound { get; set; }

    }

    public class ResultFoundClasses
    {
        public clsOntologyItem Result { get; set; }
        public FoundClasses FoundClasses { get; set; }
    }

    public class ParsePartItem
    {
        public ParsePartItem BasePart { get; set; }
        public clsOntologyItem PartOItem { get; set; }
        public string Part { get; set; }
        public int StartIx { get; set; }
        public int EndIx { get; set; }
        public int NextOrderId { get; set; }
        public List<FoundWord> Words { get; set; }

    }
}
