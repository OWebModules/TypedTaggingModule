﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class ConvertTypedTagsToRelationsModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsObjectRel> ConfigsToOntologies { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> AutomaticRelatedItems { get; set; } = new List<clsObjectRel>();

        public List<clsOntologyItem> RelatedClasses { get; set; } = new List<clsOntologyItem>();
        public clsObjectAtt DeleteOtherRelations { get; set; }
    }
}
