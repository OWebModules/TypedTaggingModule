﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    [KendoGridConfig(width = "100%", groupbable = true, autoBind = true, selectable = SelectableType.cell)]
    [KendoPageable(refresh = true, pageSizes = new int[] { 5, 10, 15, 20, 50, 100, 1000 }, buttonCount = 5, pageSize = 20)]
    [KendoSortable(mode = SortType.multiple, showIndexes = true)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    public class TypedTag
    {
        [KendoColumn(hidden = true)]
        public string IdTypedTag { get; set; }
        [KendoColumn(hidden = false, title = "Typed-Tag", Order = 1, filterable = true)]
        public string NameTypedTag { get; set; }

        [KendoColumn(hidden = true)]
        public string IdTagSource { get; set; }

        [KendoColumn(hidden = true)]
        public string IdParentTagSource { get; set; }

        [KendoColumn(hidden = false)]
        public string NameParentTagSource { get; set; }

        [KendoColumn(hidden = false, title = "Source", Order = 0, filterable = true)]
        public string NameTagSource { get; set; }

        [KendoColumn(hidden = true)]
        public string IdTag { get; set; }
        [KendoColumn(hidden = false, title = "Tag", Order = 2, filterable = true)]
        public string NameTag { get; set; }
        [KendoColumn(hidden = true)]
        public string EncodedNameTag { get; set; }

        [KendoColumn(hidden = false, title = "Tag-Type", Order = 3, filterable = true)]
        public string TagType { get; set; }

        [KendoColumn(hidden = true)]
        public string IdTagParent { get; set; }

        [KendoColumn(hidden = false, title = "Tag-Parent", Order = 4, filterable = true)]
        public string NameTagParent { get; set; }

        [KendoColumn(hidden = false, title = "OrderId", Order = 5, filterable = true)]
        public long OrderId { get; set; }

        public string UrlModuleStarter { get; set; }

    }
}
