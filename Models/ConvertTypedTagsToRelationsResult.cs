﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class ConvertTypedTagsToRelationsResult
    {
        public int CountTypedTagsChecked { get; set; }
    }
}
