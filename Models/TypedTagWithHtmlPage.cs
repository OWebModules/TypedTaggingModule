﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class TypedTagWithHtmlPage : TypedTag
    {
        public string RefUrl { get; set; }
        public TypedTagWithHtmlPage(TypedTag typedTag, string refUrl, string urlModuleStarter)
        {
            this.IdParentTagSource = typedTag.IdParentTagSource;
            this.NameParentTagSource = typedTag.NameParentTagSource;
            this.IdTag = typedTag.IdTag;
            this.IdTagParent = typedTag.IdTagParent;
            this.IdTagSource = typedTag.IdTagSource;
            this.IdTypedTag = typedTag.IdTypedTag;
            this.NameTag = typedTag.NameTag;
            this.NameTagParent = typedTag.NameTagParent;
            this.NameTagSource = typedTag.NameTagSource;
            this.NameTypedTag = typedTag.NameTypedTag;
            this.OrderId = typedTag.OrderId;
            this.TagType = typedTag.TagType;
            this.RefUrl = refUrl;
            this.UrlModuleStarter = $"{ urlModuleStarter }?Object={typedTag.IdTag}";
        }
    }
}
