﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class ChangedTypedTag
    {
        public string Uid { get; set; }
        public string PropertyName { get; set; }
        public TypedTag TypedTag { get; set; }
    }
}
