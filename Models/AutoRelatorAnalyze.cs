﻿using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class AutoRelatorAnalyze
    {
        public string IdAnalyze { get; set; }
        public DateTime AnalyzeDate { get; set; }
        public string AnalyzeText { get; set; }
        public string IdRefItem { get; set; }
        public string NameRefItem { get; set; }
        public List<FoundTerm> FoundTerms { get; set; } = new List<FoundTerm>();
        public List<FoundCombination> FoundCombinations { get; set; } = new List<FoundCombination>();
        public List<FoundRegex> FoundRegex { get; set; } = new List<Models.FoundRegex>();
        public List<TextRange> TextRanges { get; set; } = new List<TextRange>();

        public List<FoundText> FoundTexts { get; set; } = new List<FoundText>();

        public List<TextPart> TextParts { get; set; } = new List<TextPart>();

        public long CountNotExactNameSearch { get; set; }
    }

    public class AutoRelatorOntologyAnalyze
    {
        public string IdAnalyze { get; set; }
        public DateTime AnalyzeDate { get; set; }
        public string AnalyzeText { get; set; }

        public string IdRefItem { get; set; }

        public string NameRefItem { get; set; }

        public List<FoundOntologyJoin> OntologyJoins { get; set; } = new List<FoundOntologyJoin>();


        public List<TextPart> TextParts { get; set; } = new List<TextPart>();

        public void GenerateTextRanges()
        {
            OntologyJoins.ForEach(oJoin =>
            {
                var textRange = new TextRange
                {
                    RangeRefItems = new List<TextPart>()

                };
                if (oJoin.TextPart1 != null)
                {
                    textRange.RangeRefItems.Add(oJoin.TextPart1);
                }
                if (oJoin.TextPart2 != null)
                {
                    textRange.RangeRefItems.Add(oJoin.TextPart2);
                }
                


            });
        }
    }

    public class FoundOntologyJoin
    {
        public string Id { get; set; }
        public OntologyJoin OntologyJoin { get; set; }
        public TextPart TextPart1 { get; set; }
        public TextPart TextPart2 { get; set; }
        public FoundWord RelationWord { get; set; }
        public bool Exists { get; set; }

        public FoundOntologyJoin()
        {
            Id = Guid.NewGuid().ToString();
        }
    }

    public class FoundTerm
    {
        public FoundWord FoundWord { get; set; }
        public FoundCombination FoundCombination { get; set; }
        public clsOntologyItem ObjectItem { get; set; }
        public clsOntologyItem ClassItem { get; set; }

        public double Similarity { get; set; }

        public bool FoundByClass { get; set; }

        public bool FoundInText { get; set; }

    }

    public class TextPart
    {
        public string IdTextPart { get; set; }
        public string Text { get; set; }
        public string IdObject { get; set; }
        public string IdClass { get; set; }
        public string NameClass { get; set; }
        public int IxStart { get; set; }
        public int IxEnd { get; set; }

        public double Similarity { get; set; }

        public bool FoundByClass { get; set; }

        public bool FoundInText { get; set; }

        public bool FoundByRegex { get; set; }

        private clsOntologyItem objectItem;
       
        public clsOntologyItem GetObjectItem(long orderId)
        {
            if (objectItem != null)
            {
                objectItem.Val_Long = orderId;
            }
            return objectItem;
        }

        public TextPart(clsOntologyItem objectItem)
        {
            IdTextPart = Guid.NewGuid().ToString();
            this.objectItem = objectItem;
        }
    }

    public class FoundCombination
    {
        public FoundWord FirstWord { get; set; }
        public FoundWord SecondWord { get; set; }
        public clsOntologyItem ObjectItem { get; set; }
        public clsOntologyItem ClassItem { get; set; }
        public string Text { get; set; }
    }

    public class FoundRegex
    {
        public RegexToClass RegexToClass { get; set; }
        public Match Match { get; set; }
        public clsOntologyItem ObjectItem { get; set; }
        public clsOntologyItem ClassItem { get; set; }
    }

    public class FoundText
    {
        public clsOntologyItem ObjectItem { get; set; }
        public clsOntologyItem ClassItem { get; set; }
    }
}
