﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class ObjectClass
    {
        public clsOntologyItem ObjectItem { get; set; }
        public clsOntologyItem ClassItem { get; set; }
    }
}
