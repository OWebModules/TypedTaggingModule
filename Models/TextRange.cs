﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class TextRange
    {
        public List<TextPart> RangeRefItems { get; set; } = new List<TextPart>();
        public bool IsContained(int ixStart, int ixEnd)
        {
            return RangeRefItems.Any(rangeRefItem => (ixStart >= rangeRefItem.IxStart && ixStart <= rangeRefItem.IxEnd) || (ixEnd >= rangeRefItem.IxStart && ixEnd <= rangeRefItem.IxEnd));
        }
    }

}
