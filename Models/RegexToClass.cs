﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class RegexToClass
    {
        public string IdRegexToClass { get; set; }
        public string NameRegexToClass { get; set; }
        public string IdAttributeTypeRegex { get; set; }
        public string Regex { get; set; }
        public string IdClass { get; set; }
        public string NameClass { get; set; }

        public string IdClassParent { get; set; }

        public List<Match> LastMatches { get; private set; } = new List<Match>();

        public List<Match> FindMatches(string text)
        {
            var regexItem = new Regex(Regex);
            var result = new List<Match>();
            foreach(Match match in regexItem.Matches(text))
            {
                result.Add(match);
            }

            LastMatches = result;
            return result;
        }
    }
}
