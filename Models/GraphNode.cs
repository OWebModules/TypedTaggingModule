﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class GraphNode
    {
        public string id { get; set; }
        public string name { get; set; }

        [JsonIgnore]
        public string IdParent { get; set; }


        public GraphNodeData data { get; set; }

        public List<GraphNodeAdjacencies> adjacencies { get; set; } = new List<GraphNodeAdjacencies>();
    }

    public class GraphNodeData
    {
        [JsonProperty(PropertyName = "$color")]
        public string color { get; set; }

        [JsonProperty(PropertyName = "$type")]
        public string type { get; set; }

        [JsonProperty(PropertyName = "$dim")]
        public long dim { get; set; }
    }

    public class GraphNodeAdjacenciesData
    {
        [JsonProperty(PropertyName = "$color")]
        public string color { get; set; }
       
    }

    public class GraphNodeAdjacencies
    {
        public string nodeTo { get; set; }
        public string nodeFrom { get; set; }

        public GraphNodeAdjacenciesData data { get; set; }
    }

    
}
