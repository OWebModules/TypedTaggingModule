﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class PossibleRelations
    {
        public List<clsObjectRel> PossibleORelations { get; set; } = new List<clsObjectRel>();
        public List<GraphNode> PossibleRelationNodes { get; set; } = new List<GraphNode>();
    }
}
