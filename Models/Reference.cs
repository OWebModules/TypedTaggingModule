﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class Reference
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<TypedTagWithHtmlPage> Tags { get; set; }
    }
}
