﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class ResultGetTypedTagsByTaggingSource
    {
        public List<clsObjectRel> TypedTagsToTaggingSources { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> TypedTagsToTags { get; set; } = new List<clsObjectRel>();
    }
}
