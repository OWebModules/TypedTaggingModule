﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule.Models
{
    public class RankedWord
    {
        public string Word { get; set; }

        private int count;
        public int Count 
        { 
            get { return count; }
            set
            {
                count = value;
                DecisionStrength = count * rank;
            }
        }

        private int rank;
        public int Rank 
        { 
            get { return rank; }
            set 
            {
                rank = value;
                DecisionStrength = count * rank;
            }
        }

        public int DecisionStrength { get; private set; }
    }
}
