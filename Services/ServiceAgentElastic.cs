﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;
using TypedTaggingModule.Validation;

namespace TypedTaggingModule.Services
{
    public class ServiceAgentElastic : NotifyPropertyChange
    {
        private Globals globals;

        private object serviceLocker = new object();

        private clsTransaction transaction;
        private clsRelationConfig relationConfig;

        private ServiceResult serviceResultTagsByTaggingSources;
        public ServiceResult ServiceResultTagsByTaggingSources
        {
            get
            {
                lock (serviceLocker)
                {
                    return serviceResultTagsByTaggingSources;
                }

            }
            set
            {
                lock (serviceLocker)
                {
                    serviceResultTagsByTaggingSources = value;
                }

                RaisePropertyChanged(nameof(ServiceResultTagsByTaggingSources));
            }
        }

        private ServiceResult serviceResultSaveTags;
        public ServiceResult ServiceResultSaveTags
        {
            get
            {
                lock (serviceLocker)
                {
                    return serviceResultSaveTags;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    serviceResultSaveTags = value;
                    RaisePropertyChanged(nameof(ServiceResultSaveTags));
                }
            }
        }

        private clsOntologyItem serviceResultDeleteTag;
        public clsOntologyItem ServiceResultDeleteTag
        {
            get
            {
                lock (serviceLocker)
                {
                    return serviceResultDeleteTag;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    serviceResultDeleteTag = value;
                    RaisePropertyChanged(nameof(ServiceResultDeleteTag));
                }
            }
        }

        private ResultReorderTags serviceResultReorderTypedTags;
        public ResultReorderTags ServiceResultReorderTypedTags
        {
            get
            {
                lock (serviceLocker)
                {
                    return serviceResultReorderTypedTags;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    serviceResultReorderTypedTags = value;
                    RaisePropertyChanged(nameof(ServiceResultReorderTypedTags));
                }
            }
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetSimilarRelations(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var searchSimilarRelations = (from obj1 in objects
                                              from obj2 in objects
                                              select new clsObjectRel
                                              {
                                                  ID_Parent_Object = obj1.GUID_Parent,
                                                  ID_Parent_Other = obj2.GUID_Parent
                                              }).ToList();

                var dbReaderRelations = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRelations.GetDataObjectRel(searchSimilarRelations);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the similar relations!";
                    return result;
                }

                result.Result = dbReaderRelations.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ConvertTypedTagsToRelationsModel>> GetConvertTypedTagsToRelationsModel(ConvertTypedTagsToRelationsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ConvertTypedTagsToRelationsModel>>(() =>
           {
               var result = new ResultItem<ConvertTypedTagsToRelationsModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ConvertTypedTagsToRelationsModel()
               };

               var searchConfigs = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig
                   }
               };

               var dbReaderConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigs.GetDataObjects(searchConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Configs!";
                   return result;
               }

               result.ResultState = ValidationController.ValidateAndSetConvertTypedTagsToRelationsModel(result.Result, dbReaderConfigs, nameof(ConvertTypedTagsToRelationsModel.Config), globals);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchOntologies = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Convert_Typed_Tags_To_Relations_belonging_Destination_Ontologies.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Convert_Typed_Tags_To_Relations_belonging_Destination_Ontologies.ID_Class_Right
                   }
               };

               var dbReaderOntologies = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderOntologies.GetDataObjectRel(searchOntologies);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Ontologies!";
                   return result;
               }

               result.ResultState = ValidationController.ValidateAndSetConvertTypedTagsToRelationsModel(result.Result, dbReaderOntologies, nameof(ConvertTypedTagsToRelationsModel.ConfigsToOntologies), globals);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchDeleteOtherRelations = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_AttributeType = ConvertRelations.Config.LocalData.AttributeType_Delete_non_typed_relations.GUID
                   }
               };

               var dbReaderDeleteOtherRelations = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderDeleteOtherRelations.GetDataObjectAtt(searchDeleteOtherRelations);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Attribute to mark the delete of other Relations!";
                   return result;
               }

               result.ResultState = ValidationController.ValidateAndSetConvertTypedTagsToRelationsModel(result.Result, dbReaderDeleteOtherRelations, nameof(ConvertTypedTagsToRelationsModel.DeleteOtherRelations), globals);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchRelatedClasses = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = ConvertRelations.Config.LocalData.ClassRel_Convert_Typed_Tags_To_Relations_belonging_Class.ID_RelationType
                   }
               };

               var dbReaderRelatedClasses = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRelatedClasses.GetDataObjectRel(searchRelatedClasses);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the related Classes!";
                   return result;
               }

               result.ResultState = ValidationController.ValidateAndSetConvertTypedTagsToRelationsModel(result.Result, dbReaderRelatedClasses, nameof(ConvertTypedTagsToRelationsModel.RelatedClasses), globals);
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });
            return taskResult;
        }

        public async Task<clsOntologyItem> RelateTypedTags(clsOntologyItem tagSource, List<clsOntologyItem> typedTags)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();
                var relationConfig = new clsRelationConfig(globals);

                if (!typedTags.Any())
                {
                    return result;
                }

                var searchRelations = typedTags.Select(typedTag => new clsObjectRel
                {
                    ID_Object = typedTag.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Typed_Tag_is_Tagging.ID_RelationType,
                    ID_Other = tagSource.GUID
                }).ToList();

                var dbConnector = new OntologyModDBConnector(globals);

                result = dbConnector.GetDataObjectRel(searchRelations);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting existing relations";
                    return result;
                }

                var toSave = (from typedTag in typedTags
                              join foundRel in dbConnector.ObjectRels on typedTag.GUID equals foundRel.ID_Object into foundRels
                              from foundRel in foundRels.DefaultIfEmpty()
                              where foundRel == null
                              select typedTag).Select(typedTag => relationConfig.Rel_ObjectRelation(typedTag, tagSource, Config.LocalData.RelationType_is_Tagging, orderId: typedTag.Val_Long.Value)).ToList();

                if (toSave.Any())
                {
                    result = dbConnector.SaveObjRel(toSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving relations to reference";
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ServiceResult> GetTypedTagsByTags(List<clsOntologyItem> tags)
        {
            var serviceResult = new ServiceResult
            {
                Result = globals.LState_Success.Clone()
            };

            var searchResult1 = await GetTypedTagsToTags(tags);

            if (searchResult1.Result.GUID == globals.LState_Error.GUID)
            {
                serviceResult.Result = globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.TypedTagsToTags = searchResult1.ResultList;

            var searchResult2 = await GetTypedTagsToTaggingSources(serviceResult.TypedTagsToTags);

            if (searchResult2.Result.GUID == globals.LState_Error.GUID)
            {
                serviceResult.Result = globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.TypedTagsToTaggingSources = searchResult2.ResultList;

            ServiceResultTagsByTaggingSources = serviceResult;
            return serviceResult;
        }

        public async Task<ServiceResult> GetTypedTagsByTaggingSources(List<clsOntologyItem> taggingSources)
        {
            var serviceResult = new ServiceResult
            {
                Result = globals.LState_Success.Clone()
            };

            var searchResult1 = await GetTypedTagsToTaggingSource(taggingSources);

            if (searchResult1.Result.GUID == globals.LState_Error.GUID)
            {
                serviceResult.Result = globals.LState_Error.Clone();
                serviceResult.Result.Additional1 = "Error while getting the Tagging-Sources!";
                return serviceResult;
            }

            serviceResult.TypedTagsToTaggingSources = searchResult1.ResultList;

            var searchResult2 = await GetTypedTagsToTags(serviceResult.TypedTagsToTaggingSources);

            if (searchResult2.Result.GUID == globals.LState_Error.GUID)
            {
                serviceResult.Result = globals.LState_Error.Clone();
                serviceResult.Result.Additional1 = "Error while getting the ´Typed Tags!";
                return serviceResult;
            }

            serviceResult.TypedTagsToTags = searchResult2.ResultList;

            ServiceResultTagsByTaggingSources = serviceResult;
            return serviceResult;
        }

        private async Task<SearchResult<clsObjectRel>> GetTypedTagsToTaggingSource(List<clsOntologyItem> taggingSources)
        {
            var taskResult = await Task.Run<SearchResult<clsObjectRel>>(() =>
            {
                var searchResult = new SearchResult<clsObjectRel>
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchTypedTags = taggingSources.Select(taggingSource => new clsObjectRel
                {
                    ID_Other = taggingSource.GUID,
                    ID_Parent_Other = taggingSource.GUID_Parent,
                    ID_RelationType = Config.LocalData.ClassRel_Typed_Tag_is_Tagging.ID_RelationType
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                if (searchTypedTags.Any())
                {
                    var result = dbReader.GetDataObjectRel(searchTypedTags);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        searchResult.Result = globals.LState_Error.Clone();
                        return searchResult;
                    }

                    searchResult.ResultList = dbReader.ObjectRels;
                    return searchResult;
                }
                else
                {
                    searchResult.ResultList = new List<clsObjectRel>();
                    return searchResult;
                }
            });

            return taskResult;
        }

        private async Task<SearchResult<clsObjectRel>> GetTypedTagsToTags(List<clsOntologyItem> tags)
        {
            var taskResult = await Task.Run<SearchResult<clsObjectRel>>(() =>
            {
                var searchResult = new SearchResult<clsObjectRel>
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchTypedTags = tags.Select(taggingSource => new clsObjectRel
                {
                    ID_Other = taggingSource.GUID,
                    ID_Parent_Other = taggingSource.GUID_Parent,
                    ID_RelationType = Config.LocalData.ClassRel_Typed_Tag_belonging_tag.ID_RelationType
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                if (searchTypedTags.Any())
                {
                    var result = dbReader.GetDataObjectRel(searchTypedTags);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        searchResult.Result = globals.LState_Error.Clone();
                        return searchResult;
                    }

                    searchResult.ResultList = dbReader.ObjectRels;
                    return searchResult;
                }
                else
                {
                    searchResult.ResultList = new List<clsObjectRel>();
                    return searchResult;
                }
            });

            return taskResult;
        }

        private async Task<SearchResult<clsObjectRel>> GetTypedTagsToTags(List<clsObjectRel> typedTagsToTaggingSources)
        {
            var taskResult = await Task.Run<SearchResult<clsObjectRel>>(() =>
            {
                var searchResult = new SearchResult<clsObjectRel>
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchTags = typedTagsToTaggingSources.Select(typedTag => new clsObjectRel
                {
                    ID_Object = typedTag.ID_Object,
                    ID_Parent_Object = typedTag.ID_Parent_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Typed_Tag_belonging_tag.ID_RelationType
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                if (searchTags.Any())
                {
                    var result = dbReader.GetDataObjectRel(searchTags);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        searchResult.Result = globals.LState_Error.Clone();
                        return searchResult;
                    }

                    searchResult.ResultList = dbReader.ObjectRels;
                    return searchResult;
                }
                else
                {
                    searchResult.ResultList = new List<clsObjectRel>();
                    return searchResult;
                }
            });

            return taskResult;
        }

        private async Task<SearchResult<clsObjectRel>> GetTypedTagsToTaggingSources(List<clsObjectRel> typedTagsToTags)
        {
            var taskResult = await Task.Run<SearchResult<clsObjectRel>>(() =>
            {
                var searchResult = new SearchResult<clsObjectRel>
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchTaggingSources = typedTagsToTags.Select(typedTag => new clsObjectRel
                {
                    ID_Object = typedTag.ID_Object,
                    ID_Parent_Object = typedTag.ID_Parent_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Typed_Tag_is_Tagging.ID_RelationType
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                if (searchTaggingSources.Any())
                {
                    var result = dbReader.GetDataObjectRel(searchTaggingSources);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        searchResult.Result = globals.LState_Error.Clone();
                        return searchResult;
                    }

                    searchResult.ResultList = dbReader.ObjectRels;
                    return searchResult;
                }
                else
                {
                    searchResult.ResultList = new List<clsObjectRel>();
                    return searchResult;
                }
            });

            return taskResult;
        }

        public async Task<ResultItem<clsObjectAtt>> GetObjectAttribute(clsOntologyItem oItem, clsOntologyItem attributeItem)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectAtt>>(() =>
            {
                var result = new ResultItem<clsObjectAtt>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);

                var searchObjectAttribute = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = oItem.GUID,
                        ID_AttributeType = attributeItem.GUID
                    }
                };

                result.ResultState = dbReader.GetDataObjectAtt(searchObjectAttribute);

                if (!dbReader.ObjAtts.Any())
                {
                    result.ResultState = globals.LState_Nothing.Clone();
                }

                result.Result = dbReader.ObjAtts.FirstOrDefault();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<RegexToClassItems>> GetRegexToClasses()
        {
            var taskResult = await Task.Run<ResultItem<RegexToClassItems>>(() =>
             {
                 var result = new ResultItem<RegexToClassItems>
                 {
                     ResultState = globals.LState_Success.Clone()
                 };

                 var searchRegexToClasses = new List<clsOntologyItem>
                 {
                     new clsOntologyItem
                     {
                         GUID_Parent = Config.LocalData.Class_Regular_Expression_To_Class.GUID
                     }
                 };

                 var dbReaderRegexToClass = new OntologyModDBConnector(globals);

                 result.ResultState = dbReaderRegexToClass.GetDataObjects(searchRegexToClasses);

                 if (result.ResultState.GUID == globals.LState_Error.GUID)
                 {
                     return result;
                 }

                 var searchRegexToClassItems = dbReaderRegexToClass.Objects1.Select(obj => new clsObjectRel
                 {
                     ID_Object = obj.GUID,
                     ID_RelationType = Config.LocalData.RelationType_belonging_Class.GUID
                 }).ToList();

                 var dbReaderRegexToClasses = new OntologyModDBConnector(globals);

                 if (searchRegexToClassItems.Any())
                 {
                     result.ResultState = dbReaderRegexToClasses.GetDataObjectRel(searchRegexToClassItems);
                     if (result.ResultState.GUID == globals.LState_Error.GUID)
                     {
                         return result;
                     }
                 }

                 var searchRegexAtt = dbReaderRegexToClass.Objects1.Select(obj => new clsObjectAtt
                 {
                     ID_Object = obj.GUID,
                     ID_AttributeType = Config.LocalData.ClassAtt_Regular_Expression_To_Class_RegEx.ID_AttributeType
                 }).ToList();

                 var dbReaderRegexRegex = new OntologyModDBConnector(globals);

                 if (searchRegexAtt.Any())
                 {
                     result.ResultState = dbReaderRegexRegex.GetDataObjectAtt(searchRegexAtt);
                     if (result.ResultState.GUID == globals.LState_Error.GUID)
                     {
                         return result;
                     }
                 }

                 result.Result = new RegexToClassItems
                 {
                     RegexToClasses = dbReaderRegexToClass.Objects1,
                     RegexItems = dbReaderRegexRegex.ObjAtts,
                     RelatedClasses = dbReaderRegexToClasses.ObjectRels
                 };

                 return result;
             });

            return taskResult;
        }

        public async Task<ResultItem<List<ObjectClass>>> SearchObjects(List<clsOntologyItem> searchObjects)
        {
            var taskResult = await Task.Run<ResultItem<List<ObjectClass>>>(() =>
            {
                var result = new ResultItem<List<ObjectClass>>
                {
                    ResultState = globals.LState_Success.Clone()
                };


                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjects(searchObjects);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchClasses = dbReader.Objects1.GroupBy(obj => obj.GUID_Parent).Select(grp => new clsOntologyItem { GUID = grp.Key }).ToList();

                var dbReaderClasses = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderClasses.GetDataClasses(searchClasses);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = (from obj in dbReader.Objects1
                                 join objSearch in searchObjects on obj.Name.ToLower() equals objSearch.Name.ToLower()
                                 join cls in dbReaderClasses.Classes1 on obj.GUID_Parent equals cls.GUID
                                 select new ObjectClass
                                 {
                                     ObjectItem = obj,
                                     ClassItem = cls
                                 }).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsClassRel>>> GetClassRelations(List<clsClassRel> search)
        {
            var taskResult = await Task.Run<ResultItem<List<clsClassRel>>>(() =>
            {
                var result = new ResultItem<List<clsClassRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsClassRel>()
                };
                if (search.Any())
                {
                    var dbReader = new OntologyModDBConnector(globals);

                    result.ResultState = dbReader.GetDataClassRel(search);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result = dbReader.ClassRels;
                }


                return result;
            });

            return taskResult;

        }

        public async Task<ResultItem<List<clsObjectRel>>> GetObjectRelations(List<clsObjectRel> search)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };
                if (search.Any())
                {
                    var dbReader = new OntologyModDBConnector(globals);

                    result.ResultState = dbReader.GetDataObjectRel(search);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result = dbReader.ObjectRels;
                }


                return result;
            });

            return taskResult;

        }
        public async Task<ResultItem<List<FoundTerm>>> SearchObjects(string textToAnalyze, List<FoundWord> guids, string idClassForSearch)
        {
            var taskResult = await Task.Run<ResultItem<List<FoundTerm>>>(() =>
            {
                var result = new ResultItem<List<FoundTerm>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<FoundTerm>()
                };


                
                var textParts = Regex.Split(textToAnalyze, ParseParts.PunctuationMarkPattern);
                var isGuid = globals.is_GUID(textToAnalyze);
                if (!isGuid)
                {
                    guids.ForEach(guid =>
                    {
                        textToAnalyze = textToAnalyze.Replace(guid.Word, "");
                    });
                    var searchObjects = textParts.Select(textPart => new clsOntologyItem
                    {
                        GUID = isGuid ? textPart : null,
                        Name = !isGuid ? textPart : null
                    }).ToList();

                    var dbReader = new OntologyModDBConnector(globals);

                    result.ResultState = dbReader.GetDataObjects(searchObjects, maxCount: 1000);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while searching objects!";
                        return result;
                    }

                    var searchClasses = dbReader.Objects1.GroupBy(obj => obj.GUID_Parent).Select(grp => new clsOntologyItem { GUID = grp.Key }).Where(cls => cls.GUID != idClassForSearch && !string.IsNullOrEmpty(cls.GUID) && globals.is_GUID(cls.GUID)).ToList();
                    if (!string.IsNullOrEmpty(idClassForSearch))
                    {
                        searchClasses.Insert(0, new clsOntologyItem { GUID = idClassForSearch });
                    }

                    var dbReaderClasses = new OntologyModDBConnector(globals);
                    var pos = 0;
                    var count = 100;

                    var classes = new List<clsOntologyItem>();

                    if (searchClasses.Count > 100 && count > 0)
                    {

                        while (pos < searchClasses.Count)
                        {
                            count = searchClasses.Count - pos;
                            if (count > 100)
                            {
                                count = 100;
                            }
                            result.ResultState = dbReaderClasses.GetDataClasses(searchClasses.GetRange(pos, count));
                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while searching classes!";
                                return result;
                            }
                            classes.AddRange(dbReaderClasses.Classes1.Select(cls => cls.Clone()));
                            pos += count;
                        }

                    }
                    else
                    {
                        result.ResultState = dbReaderClasses.GetDataClasses(searchClasses);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while searching classes!";
                            return result;
                        }

                        classes = dbReaderClasses.Classes1;
                    }

                    var classesRequest = classes.Where(cls => cls.GUID == idClassForSearch).ToList();
                    var classesNonRequest = classes.Where(cls => cls.GUID != idClassForSearch).ToList();

                    var lowerText = textToAnalyze.ToLower();
                    result.Result = new List<FoundTerm>();
                    if (classesRequest.Any())
                    {
                        classesRequest.ForEach(cls => cls.Mark = true);
                        result.Result = (from obj in dbReader.Objects1
                                         join cls in classesRequest on obj.GUID_Parent equals cls.GUID
                                         select new { obj, cls }).Select(clsObj =>
                                         {
                                             var similarity = CalculateSimilarity(clsObj.obj.Name.ToLower(), lowerText);
                                             if (similarity > 0)
                                             {
                                                 return new FoundTerm
                                                 {
                                                     ClassItem = clsObj.cls,
                                                     ObjectItem = clsObj.obj,
                                                     FoundWord = new FoundWord
                                                     {
                                                         Word = clsObj.obj.Name,
                                                         StartIx = 0,
                                                         EndIx = clsObj.obj.Name.Length,
                                                         OItemWord = clsObj.obj
                                                     },
                                                     Similarity = similarity,
                                                     FoundByClass = true
                                                 };
                                             }
                                             return null;
                                         }).Where(fw => fw != null).ToList();
                    }

                    result.Result.AddRange((from obj in dbReader.Objects1
                                            join cls in classesNonRequest on obj.GUID_Parent equals cls.GUID
                                            select new { obj, cls }).Select(clsObj =>
                                            {
                                                var similarity = CalculateSimilarity(lowerText, clsObj.obj.Name.ToLower());
                                                var ixFound = lowerText.IndexOf(clsObj.obj.Name.ToLower());
                                                if (similarity > 0)
                                                {
                                                    return new FoundTerm
                                                    {
                                                        ClassItem = clsObj.cls,
                                                        ObjectItem = clsObj.obj,
                                                        FoundWord = new FoundWord
                                                        {
                                                            Word = similarity == 1 ? lowerText.Substring(ixFound, clsObj.obj.Name.Length) : clsObj.obj.Name,
                                                            StartIx = ixFound > -1 ? ixFound : 0,
                                                            EndIx = ixFound > -1 ? ixFound + clsObj.obj.Name.Length : 0,
                                                            OItemWord = clsObj.obj
                                                        },
                                                        Similarity = similarity,
                                                        FoundInText = ixFound > -1
                                                    };
                                                }
                                                return null;
                                            }).Where(fw => fw != null));
                }
                

                if (guids.Any())
                {
                    var searchGuids = guids.Select(guid => new clsOntologyItem
                    {
                        GUID = guid.Word
                    }).ToList();

                    var dbReaderGuids = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderGuids.GetDataObjects(searchGuids);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while searching objects!";
                        return result;
                    }

                    var searchClasses = dbReaderGuids.Objects1.Select(obj => new clsOntologyItem { GUID = obj.GUID_Parent }).ToList();

                    var dbReaderClasses = new OntologyModDBConnector(globals);
                    var classes = new List<clsOntologyItem>();
                    if (searchClasses.Any())
                    {
                        result.ResultState = dbReaderClasses.GetDataClasses(searchClasses);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while searching classes!";
                            return result;
                        }
                        classes = dbReaderClasses.Classes1;
                    }

                    result.Result.AddRange(from word in guids
                                           from objectItem in dbReaderGuids.Objects1
                                           where objectItem.GUID.ToLower() == word.Word.ToLower()
                                           join classItem in classes on objectItem.GUID_Parent equals classItem.GUID
                                           select new FoundTerm
                                           {
                                               FoundWord = word,
                                               ObjectItem = objectItem,
                                               ClassItem = classItem,
                                               Similarity = 1.0
                                           });
                }

                return result;
            });

            return taskResult;
        }

        /// <summary>
        /// Calculate percentage similarity of two strings
        /// <param name="source">Source String to Compare with</param>
        /// <param name="target">Targeted String to Compare</param>
        /// <returns>Return Similarity between two strings from 0 to 1.0</returns>
        /// </summary>
        private double CalculateSimilarity(string source, string target)
        {
            if ((source == null) || (target == null)) return 0.0;
            if ((source.Length == 0) || (target.Length == 0)) return 0.0;
            if (source == target) return 1.0;

            int stepsToSame = ComputeLevenshteinDistance(source, target);
            return (1.0 - ((double)stepsToSame / (double)Math.Max(source.Length, target.Length)));
        }

        /// <summary>
        /// Returns the number of steps required to transform the source string
        /// into the target string.
        /// </summary>
        private int ComputeLevenshteinDistance(string source, string target)
        {
            if ((source == null) || (target == null)) return 0;
            if ((source.Length == 0) || (target.Length == 0)) return 0;
            if (source == target) return source.Length;

            int sourceWordCount = source.Length;
            int targetWordCount = target.Length;

            // Step 1
            if (sourceWordCount == 0)
                return targetWordCount;

            if (targetWordCount == 0)
                return sourceWordCount;

            int[,] distance = new int[sourceWordCount + 1, targetWordCount + 1];

            // Step 2
            for (int i = 0; i <= sourceWordCount; distance[i, 0] = i++) ;
            for (int j = 0; j <= targetWordCount; distance[0, j] = j++) ;

            for (int i = 1; i <= sourceWordCount; i++)
            {
                for (int j = 1; j <= targetWordCount; j++)
                {
                    // Step 3
                    int cost = (target[j - 1] == source[i - 1]) ? 0 : 1;

                    // Step 4
                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }

            return distance[sourceWordCount, targetWordCount];
        }

        public async Task<ResultItem<List<FoundTerm>>> SearchObjects(List<FoundWord> foundWords, List<FoundWord> guids)
        {
            var taskResult = await Task.Run<ResultItem<List<FoundTerm>>>(() =>
            {
                var result = new ResultItem<List<FoundTerm>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchObjects = foundWords.GroupBy(word => word.Word).Select(word => new clsOntologyItem
                {
                    Name = word.Key
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjects(searchObjects);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchClasses = dbReader.Objects1.GroupBy(obj => obj.GUID_Parent).Select(grp => new clsOntologyItem { GUID = grp.Key }).ToList();

                var dbReaderClasses = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderClasses.GetDataClasses(searchClasses);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = (from word in foundWords
                                 from objectItem in dbReader.Objects1
                                 where CalculateSimilarity(objectItem.Name.ToLower(), word.Word.ToLower())>0
                                 join classItem in dbReaderClasses.Classes1 on objectItem.GUID_Parent equals classItem.GUID
                                 select new FoundTerm
                                 {
                                     FoundWord = word,
                                     ObjectItem = objectItem,
                                     ClassItem = classItem,
                                     Similarity = CalculateSimilarity(objectItem.Name.ToLower(), word.Word.ToLower())
                                 }).ToList();

                if (guids.Any())
                {
                    var searchGuids = guids.Select(guid => new clsOntologyItem
                    {
                        GUID = guid.Word
                    }).ToList();

                    var dbReaderGuids = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderGuids.GetDataObjects(searchGuids);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.AddRange(from word in guids
                                           from objectItem in dbReaderGuids.Objects1
                                           where objectItem.GUID == word.Word
                                           join classItem in dbReaderClasses.Classes1 on objectItem.GUID_Parent equals classItem.GUID
                                           select new FoundTerm
                                           {
                                               FoundWord = word,
                                               ObjectItem = objectItem,
                                               ClassItem = classItem,
                                               Similarity = 1.0
                                           });
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetClasses(List<clsOntologyItem> objects)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchClasses = objects.Select(obj => new clsOntologyItem
                {
                    GUID = obj.GUID_Parent
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataClasses(searchClasses);
                return result;
            });

            return taskResult;
        }


        public async Task<clsOntologyItem> DeleteTypedTag(clsOntologyItem oItemTypedTag)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbDeletor = new OntologyModDBConnector(globals);
                var dbReaderLeftRight = new OntologyModDBConnector(globals);
                var dbReaderRightLeft = new OntologyModDBConnector(globals);

                var searchLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemTypedTag.GUID
                }
            };

                result = dbReaderLeftRight.GetDataObjectRel(searchLeftRight);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    ServiceResultDeleteTag = result;
                    return result;
                }

                var searchRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemTypedTag.GUID
                }
            };

                result = dbReaderRightLeft.GetDataObjectRel(searchRightLeft);

                var deleteTypedTag = true;

                if (dbReaderRightLeft.ObjectRels.Any())
                {
                    deleteTypedTag = false;
                }

                dbReaderLeftRight.ObjectRels.ForEach(objRel =>
                {
                    if (objRel.ID_Parent_Other == Config.LocalData.ClassRel_Typed_Tag_belongs_to_user.ID_Class_Right &&
                        objRel.ID_RelationType == Config.LocalData.ClassRel_Typed_Tag_belongs_to_user.ID_RelationType)
                    {
                        return;
                    }

                    if (objRel.ID_Parent_Other == Config.LocalData.ClassRel_Typed_Tag_belongs_to_Group.ID_Class_Right &&
                        objRel.ID_RelationType == Config.LocalData.ClassRel_Typed_Tag_belongs_to_Group.ID_RelationType)
                    {
                        return;
                    }

                    if (objRel.ID_RelationType == Config.LocalData.RelationType_belonging_tag.GUID)
                    {
                        return;
                    }

                    if (objRel.ID_RelationType == Config.LocalData.RelationType_is_Tagging.GUID)

                        return;

                    deleteTypedTag = false;
                });

                if (deleteTypedTag)
                {
                    result = dbDeletor.DelObjectRels(dbReaderLeftRight.ObjectRels);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        ServiceResultDeleteTag = result;
                        return result;
                    }

                    result = dbDeletor.DelObjects(new List<clsOntologyItem>
                {
                    oItemTypedTag
                });
                }

                ServiceResultDeleteTag = result;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultReorderTags> ReOrderTypedTags(List<TypedTag> typedTags)
        {
            var taskResult = await Task.Run<ResultReorderTags>(() =>
            {
                long orderId = 1;

                var searchOtherSources = typedTags.Select(typedTag => new clsObjectRel
                {
                    ID_Object = typedTag.IdTypedTag,
                    ID_RelationType = Config.LocalData.RelationType_is_Tagging.GUID
                }).ToList();

                var result = new ResultReorderTags
                {
                    Result = globals.LState_Success.Clone()
                };

                var otherRels = new List<clsObjectRel>();
                if (searchOtherSources.Any())
                {
                    var dbReaderOtherSources = new OntologyModDBConnector(globals);

                    result.Result = dbReaderOtherSources.GetDataObjectRel(searchOtherSources);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    otherRels = dbReaderOtherSources.ObjectRels;
                }

                
                var rels = typedTags.SelectMany(typedTag =>
                {
                    var relation = new List<clsObjectRel>();
                    var relation1 = relationConfig.Rel_ObjectRelation(new clsOntologyItem { GUID = typedTag.IdTypedTag, Name = typedTag.NameTypedTag, GUID_Parent = Config.LocalData.Class_Typed_Tag.GUID, Type = globals.Type_Object },
                        new clsOntologyItem { GUID = typedTag.IdTagSource, Name = typedTag.NameTagSource, GUID_Parent = typedTag.IdParentTagSource, Type = globals.Type_Object },
                        Config.LocalData.RelationType_is_Tagging,
                        orderId: orderId);

                    relation.AddRange(otherRels.Where(typedTag2 => typedTag2.ID_Object == typedTag.IdTypedTag && typedTag2.ID_Other != typedTag.IdTagSource).Select(typedOther => relationConfig.Rel_ObjectRelation(new clsOntologyItem { GUID = typedTag.IdTypedTag, Name = typedTag.NameTypedTag, GUID_Parent = Config.LocalData.Class_Typed_Tag.GUID, Type = globals.Type_Object },
                            new clsOntologyItem { GUID = typedOther.ID_Other, Name = typedOther.Name_Other, GUID_Parent = typedOther.ID_Parent_Other, Type = globals.Type_Object },
                            Config.LocalData.RelationType_is_Tagging,
                            orderId: orderId)));
                    

                    relation.Add(relation1);

                    orderId++;
                    return relation;
                }).ToList();

                var dbWriter = new OntologyModDBConnector(globals);

                result.Result = dbWriter.SaveObjRel(rels);

                var typedTagsForNewOrder = (from typedTag in typedTags
                                            join rel in rels on typedTag.IdTypedTag equals rel.ID_Object
                                            select new { typedTag, rel });
                foreach (var item in typedTagsForNewOrder)
                {
                    item.typedTag.OrderId = item.rel.OrderID.Value;
                }
                result.TypedTags = typedTags;
                ServiceResultReorderTypedTags = result;
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<OntoRelation>>> SaveOntoRelations(List<FoundOntologyJoin> ontologyJoins, clsOntologyItem refObject)
        {
            var taskResult = await Task.Run<ResultItem<List<OntoRelation>>>(() =>
            {
                var result = new ResultItem<List<OntoRelation>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var ontoRelationsLeftRight = ontologyJoins.Where(ontoJoin => ontoJoin.OntologyJoin.OItem3 != null &&
                   ontoJoin.OntologyJoin.OItem3.RefItem.Type == globals.Type_RelationType &&
                   ontoJoin.OntologyJoin.OItem1 != null && ontoJoin.OntologyJoin.OItem1.RefItem.GUID == refObject.GUID_Parent &&
                   ontoJoin.TextPart2 != null &&
                   !ontoJoin.Exists).Select(ontoJoin => new OntoRelation
                   {
                       OntologyJoin = ontoJoin,
                       Relation = new clsObjectRel
                       {
                           ID_Object = refObject.GUID,
                           ID_Parent_Object = refObject.GUID_Parent,
                           ID_RelationType = ontoJoin.OntologyJoin.OItem3.RefItem.GUID,
                           ID_Other = ontoJoin.TextPart2.IdObject,
                           ID_Parent_Other = ontoJoin.TextPart2.IdClass,
                           Ontology = globals.Type_Object,
                           OrderID = 1
                       }
                   });

                var ontoRelationsRightLeft = ontologyJoins.Where(ontoJoin => ontoJoin.OntologyJoin.OItem3 != null &&
                   ontoJoin.OntologyJoin.OItem3.RefItem.Type == globals.Type_RelationType &&
                   ontoJoin.OntologyJoin.OItem2 != null && ontoJoin.OntologyJoin.OItem2.RefItem.GUID == refObject.GUID_Parent &&
                   ontoJoin.TextPart1 != null &&
                   !ontoJoin.Exists).Select(ontoJoin => new OntoRelation
                   {
                       OntologyJoin = ontoJoin,
                       Relation = new clsObjectRel
                       {
                           ID_Other = refObject.GUID,
                           ID_Parent_Other = refObject.GUID_Parent,
                           ID_RelationType = ontoJoin.OntologyJoin.OItem3.RefItem.GUID,
                           ID_Object = ontoJoin.TextPart1.IdObject,
                           ID_Parent_Object = ontoJoin.TextPart1.IdClass,
                           Ontology = globals.Type_Object,
                           OrderID = 1
                       }
                   });


                var dbWriter = new OntologyModDBConnector(globals);

                if (ontoRelationsLeftRight.Any())
                {
                    result.ResultState = dbWriter.SaveObjRel(ontoRelationsLeftRight.Select(oRel => oRel.Relation).ToList());

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                if (ontoRelationsRightLeft.Any())
                {
                    result.ResultState = dbWriter.SaveObjRel(ontoRelationsRightLeft.Select(oRel => oRel.Relation).ToList());


                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                }

                result.Result = new List<OntoRelation>();

                result.Result.AddRange(ontoRelationsLeftRight);
                result.Result.AddRange(ontoRelationsRightLeft);
                result.Result.ForEach(rel => rel.OntologyJoin.Exists = true);
                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveRelations(List<clsObjectRel> relations)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                if (relations.Any())
                {
                    result = dbWriter.SaveObjRel(relations);
                }
                
                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving relations!";
                }
                return result;
            });

            return taskResult;
        }

        public async Task<ServiceResultRelations> SaveRelations(clsOntologyItem oItemRef, List<clsOntologyItem> references, clsOntologyItem relationType, clsOntologyItem oItemUser, clsOntologyItem oItemGroup)
        {

            var taskResult = await Task.Run<ServiceResultRelations>(() =>
            {
                var dbReaderClasses = new OntologyModDBConnector(globals);

                var searchClasses = references.GroupBy(refItm => refItm.GUID_Parent).Select(refItm => new clsOntologyItem { GUID = refItm.Key }).ToList();

                var resultItem1 = new ServiceResultRelations
                {
                    ResultState = dbReaderClasses.GetDataClasses(searchClasses)

                };

                var oItemResult = GetOItem(oItemRef.GUID_Parent, globals.Type_Class);

                var classRelations = dbReaderClasses.Classes1.Select(clsItem => relationConfig.Rel_ClassRelation(oItemResult, clsItem, relationType)).ToList();

                resultItem1.ResultState = dbReaderClasses.GetDataClassRel(classRelations.Select(clsRel => new clsClassRel { ID_Class_Left = clsRel.ID_Class_Left, ID_Class_Right = clsRel.ID_Class_Right, ID_RelationType = clsRel.ID_RelationType }).ToList());

                var classRelToSave = (from clsRelToSave in classRelations
                                      join clsRelExist in dbReaderClasses.ClassRels on new { clsRelToSave.ID_Class_Left, clsRelToSave.ID_Class_Right, clsRelToSave.ID_RelationType } equals new { clsRelExist.ID_Class_Left, clsRelExist.ID_Class_Right, clsRelExist.ID_RelationType } into clsExists
                                      from clsRelExist in clsExists.DefaultIfEmpty()
                                      where clsRelExist == null
                                      select clsRelToSave).ToList();

                if (classRelToSave.Any())
                {
                    resultItem1.ResultState = dbReaderClasses.SaveClassRel(classRelToSave);
                }

                if (resultItem1.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return resultItem1;
                }

                var relations = references.Select(refItm => relationConfig.Rel_ObjectRelation(oItemRef, refItm, relationType, orderId: refItm.Val_Long ?? 0)).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                resultItem1.ResultState = dbReader.GetDataObjectRel(relations);

                if (resultItem1.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return resultItem1;
                }

                var relationsToSave = (from rel in relations
                                       join relExist in dbReader.ObjectRels on new { rel.ID_Object, rel.ID_Other, rel.ID_RelationType } equals new { relExist.ID_Object, relExist.ID_Other, relExist.ID_RelationType } into relExists
                                       from relExist in relExists.DefaultIfEmpty()
                                       where relExist == null
                                       select rel).ToList();

                if (relationsToSave.Any())
                {
                    resultItem1.ResultState = dbReader.SaveObjRel(relationsToSave);
                }

                resultItem1.Relations = relations;

                return resultItem1;
            });

            return taskResult;

        }

        public async Task<ResultReorderTags> SaveTypedTags(clsOntologyItem oItemTagSource, List<TypedTag> typedTags)
        {

            var result = new ResultReorderTags
            {
                Result = globals.LState_Success.Clone(),
                TypedTags = typedTags
            };


            var typedTagsResult = await GetTypedTagsByTaggingSources(new List<clsOntologyItem> { oItemTagSource });

            if (typedTagsResult.Result.GUID == globals.LState_Error.GUID)
            {
                result.Result = typedTagsResult.Result;
                return result;
            }

            List<clsObjectRel> relTagSourceResult = new List<clsObjectRel>();
            List<clsObjectRel> relTagDestResult = new List<clsObjectRel>();

            var tagsToInsert = (from tag in typedTags
                                join tagPresent in ServiceResultTagsByTaggingSources.TypedTagsToTags on tag.IdTag equals tagPresent.ID_Other into tagsPresent
                                from tagPresent in tagsPresent.DefaultIfEmpty()
                                where tagPresent == null
                                select tag).ToList();

            result.TypedTags = tagsToInsert;
            if (!tagsToInsert.Any())
            {

                return result;
            }

            transaction.ClearItems();

            foreach (var typedTag in tagsToInsert)
            {

                var relTagSource = relationConfig.Rel_ObjectRelation(new clsOntologyItem { GUID = typedTag.IdTag, Name = typedTag.NameTag, GUID_Parent = typedTag.IdTagParent, Type = globals.Type_Object }, oItemTagSource, Config.LocalData.RelationType_is_Tagging, orderId: typedTag.OrderId);

                result.Result = transaction.do_Transaction(relTagSource);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    return result;
                }

            }

            return result;

        }

        public async Task<ServiceResult> SaveTypedTags(clsOntologyItem oItemTagSource, List<clsOntologyItem> tags, clsOntologyItem oItemUser, clsOntologyItem oItemGroup, IMessageOutput messageOutput)
        {

            var result = globals.LState_Success.Clone();

            messageOutput?.OutputInfo("Get TypedTags of Tagging-Source...");
            var typedTagsResult = await GetTypedTagsByTaggingSources(new List<clsOntologyItem> { oItemTagSource });

            if (typedTagsResult.Result.GUID == globals.LState_Error.GUID)
            {
                messageOutput?.OutputError(typedTagsResult.Result.Additional1);
                var resultItem = new ServiceResult
                {
                    Result = typedTagsResult.Result,
                };
                return resultItem;
            }

            messageOutput?.OutputInfo("Have TypedTags of Tagging-Source.");

            List<clsObjectRel> relTagSourceResult = new List<clsObjectRel>();
            List<clsObjectRel> relTagDestResult = new List<clsObjectRel>();

            var typedTags = tags.Where(tag => tag.GUID_Parent == Config.LocalData.Class_Typed_Tag.GUID);

            var searchTypedTagsRelated = typedTags.Select(typedTag => new clsObjectRel
            {
                ID_Object = typedTag.GUID,
                ID_RelationType = Config.LocalData.RelationType_belonging_tag.GUID
            }).ToList();

            if (searchTypedTagsRelated.Any())
            {
                messageOutput?.OutputInfo("Get Tags...");
                var dbReaderTypedTags = new OntologyModDBConnector(globals);

                result = dbReaderTypedTags.GetDataObjectRel(searchTypedTagsRelated);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the Tags!";
                    messageOutput?.OutputError(result.Additional1);
                    var resultItem = new ServiceResult
                    {
                        Result = result,
                    };
                    return resultItem;
                }

                relTagDestResult.AddRange(dbReaderTypedTags.ObjectRels);
                messageOutput?.OutputInfo("Have Tags.");

            }

            messageOutput?.OutputInfo("Determine Tags to insert...");
            var tagsToInsert = (from tag in tags.Where(tag => tag.GUID_Parent != Config.LocalData.Class_Typed_Tag.GUID)
                                join tagPresent in ServiceResultTagsByTaggingSources.TypedTagsToTags on tag.GUID equals tagPresent.ID_Other into tagsPresent
                                from tagPresent in tagsPresent.DefaultIfEmpty()
                                where tagPresent == null
                                select tag).ToList();

            tagsToInsert.AddRange(from typedTag in typedTags
                                  join tagPresent in ServiceResultTagsByTaggingSources.TypedTagsToTags on typedTag.GUID equals tagPresent.ID_Object into tagsPresent
                                  from tagPresent in tagsPresent.DefaultIfEmpty()
                                  where tagPresent == null
                                  select typedTag);

            messageOutput?.OutputInfo("Determined Tags to insert.");
            if (!tagsToInsert.Any())
            {

                ServiceResultSaveTags = new ServiceResult
                {
                    Result = result,
                    TypedTagsToTaggingSources = ServiceResultTagsByTaggingSources.TypedTagsToTaggingSources,
                    TypedTagsToTags = ServiceResultTagsByTaggingSources.TypedTagsToTags
                };
                return ServiceResultSaveTags;
            }

            transaction.ClearItems();

            messageOutput?.OutputInfo("Insert Tags...");
            foreach (var tag in tagsToInsert)
            {
                clsOntologyItem typedTag = null;
                if (tag.GUID_Parent != Config.LocalData.Class_Typed_Tag.GUID)
                {
                    typedTag = new clsOntologyItem
                    {
                        GUID = globals.NewGUID,
                        Name = tag.Name,
                        GUID_Parent = Config.LocalData.Class_Typed_Tag.GUID,
                        Type = globals.Type_Object
                    };

                    result = transaction.do_Transaction(typedTag);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        var resultItem = new ServiceResult
                        {
                            Result = result,
                        };
                        return resultItem;
                    }
                }
                else
                {
                    typedTag = tag;
                }

                var relTagSource = relationConfig.Rel_ObjectRelation(typedTag, oItemTagSource, Config.LocalData.RelationType_is_Tagging, orderId: tag.Val_Long != null ? tag.Val_Long.Value : 1);

                result = transaction.do_Transaction(relTagSource);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    var resultItem = new ServiceResult
                    {
                        Result = result,
                    };
                    return resultItem;
                }

                relTagSource.Name_Object = typedTag.Name;
                relTagSource.Name_Other = oItemTagSource.Name;

                if (tag.GUID_Parent != Config.LocalData.Class_Typed_Tag.GUID)
                {
                    var relTagDest = relationConfig.Rel_ObjectRelation(typedTag, tag, Config.LocalData.RelationType_belonging_tag);

                    result = transaction.do_Transaction(relTagDest);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        var resultItem = new ServiceResult
                        {
                            Result = result,
                        };
                        return resultItem;
                    }

                    relTagDest.Name_Other = tag.Name;

                    var rel = relationConfig.Rel_ObjectRelation(typedTag, oItemUser, Config.LocalData.RelationType_belongs_to);

                    result = transaction.do_Transaction(rel);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        var resultItem = new ServiceResult
                        {
                            Result = result,
                        };
                        return resultItem;
                    }
                    rel = relationConfig.Rel_ObjectRelation(typedTag, oItemGroup, Config.LocalData.RelationType_belongs_to);

                    result = transaction.do_Transaction(rel);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        var resultItem = new ServiceResult
                        {
                            Result = result,
                        };
                        return resultItem;
                    }

                    relTagDestResult.Add(relTagDest);
                }
                else
                {

                }



                relTagSourceResult.Add(relTagSource);

            }

            messageOutput?.OutputInfo("Inserted Tags.");

            var resultItem1 = new ServiceResult
            {
                Result = result,
                TypedTagsToTaggingSources = relTagSourceResult,
                TypedTagsToTags = relTagDestResult
            };

            ServiceResultSaveTags = resultItem1;
            return resultItem1;

        }

        public async Task<clsOntologyItem> SaveNewObjectItems(List<clsOntologyItem> objectItems)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var newObjectItems = objectItems
                    .Where(obj => string.IsNullOrEmpty(obj.GUID) || (obj.New_Item != null && obj.New_Item.Value))
                    .Select(obj => obj).ToList();

                newObjectItems.Where(obj => string.IsNullOrEmpty(obj.GUID)).ToList().ForEach(obj =>
                {
                    obj.GUID = globals.NewGUID;
                });

                if (newObjectItems.Any())
                {
                    var dbWriter = new OntologyModDBConnector(globals);

                    result = dbWriter.SaveObjects(newObjectItems);
                }


                return result;
            });

            return taskResult;
        }

        public clsOntologyItem SaveTypedTagName(clsOntologyItem oItemTypedTag)
        {
            transaction.ClearItems();

            var result = transaction.do_Transaction(oItemTypedTag);

            return result;
        }

        public clsOntologyItem ChangeOrderId(clsOntologyItem oItemTypedTag, clsOntologyItem oItemTagSource, long orderId)
        {
            transaction.ClearItems();

            var rel = relationConfig.Rel_ObjectRelation(oItemTypedTag, oItemTagSource, Config.LocalData.RelationType_is_Tagging, orderId: orderId);

            var result = transaction.do_Transaction(rel);

            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjects(List<string> objectIdList)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var searchObjects = objectIdList.Select(id => new clsOntologyItem
               {
                   GUID = id
               }).ToList();

               var dbReader = new OntologyModDBConnector(globals);

               result.ResultState = dbReader.GetDataObjects(searchObjects);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the objects!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(globals);
            return dbReader.GetOItem(id, type);

        }

        public async Task<ResultItem<List<ObjectClass>>> GetObjectClassesByName(List<clsOntologyItem> objectItems)
        {
            var taskResult = await Task.Run<ResultItem<List<ObjectClass>>>(() =>
            {
                var result = new ResultItem<List<ObjectClass>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReaderObjects = new OntologyModDBConnector(globals);

                if (objectItems.Any())
                {
                    result.ResultState = dbReaderObjects.GetDataObjects(objectItems);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                result.Result = (from cls in objectItems.SelectMany(obj => obj.OList_Rel)
                                 join obj in dbReaderObjects.Objects1 on cls.GUID equals obj.GUID_Parent
                                 select new ObjectClass
                                 {
                                     ObjectItem = obj,
                                     ClassItem = cls
                                 }).ToList();

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<List<ObjectClass>>> GetObjectsOfClasses(List<clsOntologyItem> classes, List<FoundWord> words)
        {
            var taskResult = await Task.Run<ResultItem<List<ObjectClass>>>(() =>
            {
                var result = new ResultItem<List<ObjectClass>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReaderClasses = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderClasses.GetDataClasses(classes);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchObjects = (from word in words
                                     from classItm in classes
                                     select new clsOntologyItem
                                     {
                                         GUID_Parent = classItm.GUID,
                                         Name = word.Word
                                     }).ToList();

                var dbReaderObjects = new OntologyModDBConnector(globals);

                if (searchObjects.Any())
                {
                    result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                result.Result = (from cls in dbReaderClasses.Classes1
                                 join obj in dbReaderObjects.Objects1 on cls.GUID equals obj.GUID_Parent
                                 select new ObjectClass
                                 {
                                     ObjectItem = obj,
                                     ClassItem = cls
                                 }).ToList();

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<List<ObjectClass>>> GetObjects(List<clsOntologyItem> classes, string text)
        {
            var taskResult = await Task.Run<ResultItem<List<ObjectClass>>>(() =>
            {
                var result = new ResultItem<List<ObjectClass>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchObjects = classes.Select(cls => new clsOntologyItem
                {
                    Name = text,
                    GUID_Parent = cls.GUID
                }).ToList();

                if (!classes.Any())
                {
                    searchObjects = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            Name = text
                        }
                    };
                }

                var dbReaderObjects = new OntologyModDBConnector(globals);

                if (searchObjects.Any())
                {
                    result.ResultState = dbReaderObjects.GetDataObjects(searchObjects);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                if (classes.Any())
                {
                    result.Result = (from cls in classes
                                     join obj in dbReaderObjects.Objects1 on cls.GUID equals obj.GUID_Parent
                                     select new ObjectClass
                                     {
                                         ObjectItem = obj,
                                         ClassItem = cls
                                     }).OrderBy(obj => obj.ObjectItem.Name).ToList();
                }
                else
                {
                    var addPoint = false;
                    var objects = dbReaderObjects.Objects1.OrderBy(obj => obj.Name).ToList() ;
                    if (objects.Count > 100)
                    {
                        addPoint = true;
                        objects = objects.GetRange(0, 99);
                    }

                    var dbReaderClasses = new OntologyModDBConnector(globals);
                    var searchClasses = objects.GroupBy(obj => new { GUID = obj.GUID_Parent }).Select(objGrp => objGrp.Key).Select(cls => new clsOntologyItem { GUID = cls.GUID }).ToList();

                    result.ResultState = dbReaderClasses.GetDataClasses(searchClasses);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result = (from cls in dbReaderClasses.Classes1
                                     join obj in objects on cls.GUID equals obj.GUID_Parent
                                     select new ObjectClass
                                     {
                                         ObjectItem = obj,
                                         ClassItem = cls
                                     }).ToList();
                    if (addPoint)
                    {
                        result.Result.Add(new ObjectClass
                        {
                            ObjectItem = new clsOntologyItem
                            {
                                GUID = "...",
                                Name = "..."
                            },
                            ClassItem = new clsOntologyItem()
                        });
                    }
                }
                
                

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetReferenceOfTaggingSource(string idClassTaggingSource, string idRelationType)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var searchReferencesOfTaggingSources = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = idClassTaggingSource,
                        ID_RelationType = idRelationType
                    }
                };

                var dbReaderReferencesOfTaggingSources = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReferencesOfTaggingSources.GetDataObjectRel(searchReferencesOfTaggingSources);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relations between the tagging-sources and the references";
                }

                result.Result = dbReaderReferencesOfTaggingSources.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetNonExistingRelations(List<clsObjectRel> relationsToCheck)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               var dbReaderCheck = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderCheck.GetDataObjectRel(relationsToCheck.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Object,
                   ID_RelationType = rel.ID_RelationType,
                   ID_Other = rel.ID_Other,
                   OrderID = rel.OrderID
              }).ToList());

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Erro while getting the relations!";
                   return result;
               }

               result.Result = (from relationToCheck in relationsToCheck
                                join relationInDb in dbReaderCheck.ObjectRels on new { relationToCheck.ID_Object, relationToCheck.ID_RelationType, relationToCheck.ID_Other, relationToCheck.OrderID }
                                    equals new { relationInDb.ID_Object, relationInDb.ID_RelationType, relationInDb.ID_Other, relationInDb.OrderID } into relationsInDb
                                from relationInDb in relationsInDb.DefaultIfEmpty()
                                where relationInDb == null
                                select relationToCheck).ToList();

               return result;
           });
            return taskResult;
        }

        public async Task<ResultItem<ResultGetTypedTagsByTaggingSource>> GetTypedTagsByTaggingSources(string idClassTaggingSource)
        {
            var taskResult = await Task.Run<ResultItem<ResultGetTypedTagsByTaggingSource>>(() =>
            {
                var result = new ResultItem<ResultGetTypedTagsByTaggingSource>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ResultGetTypedTagsByTaggingSource()
                };

                var searchTypedTags = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Other = idClassTaggingSource,
                        ID_Parent_Object = Config.LocalData.Class_Typed_Tag.GUID,
                        ID_RelationType = Config.LocalData.RelationType_is_Tagging.GUID
                    }
                };

                var dbReaderTypedTagsToTaggingSource = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTypedTagsToTaggingSource.GetDataObjectRel(searchTypedTags);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Typed Tags of TaggingSrouces!";
                    return result;
                }

                result.Result.TypedTagsToTaggingSources = dbReaderTypedTagsToTaggingSource.ObjectRels;

                var searchTypedTagsToTags = result.Result.TypedTagsToTaggingSources.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.RelationType_belonging_tag.GUID
                }).ToList();

                var dbReaderTypedTagsToTags = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTypedTagsToTags.GetDataObjectRel(searchTypedTags);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Typed Tags to Tags!";
                    return result;
                }

                result.Result.TypedTagsToTags = dbReaderTypedTagsToTags.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetStopWords()
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjects(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Stopp_W_rter.GUID
                    }
                });

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = dbReader.Objects1;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<long>> CountSearchText(string text)
        {
            var taskResult = await Task.Run<ResultItem<long>>(() =>
           {
               var result = new ResultItem<long>
               {
                   ResultState = globals.LState_Success.Clone()
               };

               var dbReader = new OntologyModDBConnector(globals);
               result.ResultState = dbReader.GetDataObjects(new List<clsOntologyItem> { new clsOntologyItem { Name = text } }, doCount: true);

               return result;
           });

            return taskResult;
        }
      
        public ServiceAgentElastic(Globals globals)
        {
            this.globals = globals;
            Initialize();
        }

        private void Initialize()
        {
            transaction = new clsTransaction(globals);
            relationConfig = new clsRelationConfig(globals);
        }

    }


    public class SearchResult<TType>
    {
        public clsOntologyItem Result { get; set; }
        public List<TType> ResultList { get; set; }
    }

    public class ServiceResult
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectRel> TypedTagsToTaggingSources { get; set; }
        public List<clsObjectRel> TypedTagsToTags { get; set; }
    }

    public class ResultReorderTags
    {
        public clsOntologyItem Result { get; set; }
        public List<TypedTag> TypedTags { get; set; }
    }

    public class RegexToClassItems
    {
        public List<clsOntologyItem> RegexToClasses { get; set; }
        public List<clsObjectAtt> RegexItems { get; set; }
        public List<clsObjectRel> RelatedClasses { get; set; }

    }

    public class ServiceResultRelations
    {
        public clsOntologyItem ResultState { get; set; }
        public List<clsObjectRel> Relations { get; set; }
    }

}
