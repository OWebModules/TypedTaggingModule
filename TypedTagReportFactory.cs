﻿using OntologyAppDBConnector;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedTaggingModule
{
    public class TypedTagReportFactory : IReportFactory
    {
        public event PackagePrepared OnPackagePrepared;

        public TableDefinition TableDefinition { get; private set; }
        public bool IsAspNetProject { get; set; }

        public ResultItem<TableDefinition> GetTableDefinition(Globals globals)
        {
            var result = new ResultItem<TableDefinition>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new TableDefinition()
            };

            result.Result.TableName = "orgt_TypedTags";

            return result;
        }

        public bool IsResponsible(string classId)
        {
            return (classId == Config.LocalData.Class_Typed_Tag.GUID);
        }

        public void StartPrepare()
        {
            throw new NotImplementedException();
        }
    }
}